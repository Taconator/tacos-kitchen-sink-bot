import asyncio
import discord
import sys
import traceback
from discord.ext import commands
from utils import checks


def get_prefix(bot, message):
    prefixes = ['^']

    if not message.server:
        return '^'

    return commands.when_mentioned_or(*prefixes)(bot, message)


Bot = commands.Bot(command_prefix=get_prefix, description='Taco\'s Kitchen Sink Bot Rewrite')

Extensions = [
    'cogs.memes',
    'cogs.moderation',
    'cogs.fivem',
    'cogs.owner',
    'cogs.randomcog',
    'cogs.imagecmds',
    'cogs.nsfw.nsfw',
    'cogs.ttfsroles',
    'cogs.conversion',
    'cogs.text',
    'cogs.kawaii',
    'cogs.lyrics',
    'cogs.osu',
    'cogs.ownroles.createownroles',
    # 'cogs.raven'
]

if __name__ == '__main__':
    for Extension in Extensions:
        try:
            Bot.load_extension(Extension)
        except Exception as e:
            print(f'Failed to load extension {Extension}.', file=sys.stderr)
            traceback.print_exc()


@Bot.event
async def on_ready():
    print(f'\n\nLogged in as: {Bot.user.name} - {Bot.user.id}\nVersion: {discord.__version__}\n')
    await Bot.change_presence(game=discord.Game(name='^help'))
    print(f'Successfully logged in and booted...!')


@Bot.event
async def on_command_error(Error, ctx):
    if isinstance(Error, commands.CommandOnCooldown):
        Wait = Error.retry_after
        MsgTD = await Bot.send_message(ctx.message.channel, ':x: **Command is on cooldown, retry in {} seconds**'.format(round(Error.retry_after, 2)))
        await asyncio.sleep(Wait)
        await Bot.edit_message(MsgTD, new_content=':white_check_mark: **Cooldown is over, can now use command**')
        await asyncio.sleep(3)
        await Bot.delete_messages([MsgTD, ctx.message])
        return
    elif isinstance(Error, checks.NotOwner):
        MsgTD = await Bot.send_message(ctx.message.channel, ':x: **You\'re not allowed to execute that command, silly!**')
        await asyncio.sleep(5)
        await Bot.delete_messages([MsgTD, ctx.message])
    elif isinstance(Error, commands.CommandNotFound):
        MsgTD = await Bot.send_message(ctx.message.channel, ':x: **Command entered was not found.**')
        await asyncio.sleep(5)
        await Bot.delete_messages([MsgTD, ctx.message])
    else:
        Embed = discord.Embed(title='Exception Has Occurred', description='Please report to Taconator#5417\nInclude the exact command you ran', color=0xFF0000)
        Embed.add_field(name='Exception Type', value=Error.__class__)
        Embed.add_field(name='Exception Value', value=Error.__str__())
        try:
            Embed.add_field(name='Traceback Details', value=f'Line Exception Occurred On\t{Error.__traceback__.tb_lineno}\nIndex of Last Attempted Instruction\t{Error.__traceback__.tb_lasti}\nFrame Line Number\t{Error.__traceback__.tb_frame.f_lineno}')
        except Exception:
            pass
        await Bot.send_message(ctx.message.channel, embed=Embed)


Bot.run('MzI0OTk0NjUwODIzODUyMDQz.DpVkDA.01uC0KsFiFUlvUUYAhZV_BDTF5g', bot=True, reconnect=True)