import asyncio, json, os, re
import discord
from discord.ext import commands
class CreateOwnRoles:
    Roles = None

    MAX_CUSTOM_ROLES = 5

    class AlreadyClaimed(commands.CommandError): pass
    class AlreadyTop(commands.CommandError): pass
    class WrongDiscord(commands.CommandError): pass
    class InvalidColor(commands.CommandError): pass
    class NonexistentRole(commands.CommandError): pass
    class TooManyRoles(commands.CommandError): pass
    class UnownedRole(commands.CommandError): pass
    class SharedRole(commands.CommandError): pass

    def __init__(self, Bot):
        self.Bot = Bot
        if self.FileCheck():
            self.ReadJson()

    @classmethod
    def IsValidHex(self, Hex : str):
        return re.search(r'^#(?:[0-9a-fA-F]{3}){1,2}$', Hex)

    @classmethod
    def IsTKSB(self, ServerID):
        return ServerID == '470102857777938464'

    @classmethod
    def ReadJson(self):
        with open('./cogs/ownroles/roles.json', 'r') as File:
            self.Roles = json.load(File)

    @classmethod
    def SaveJson(self):
        with open('./cogs/ownroles/roles.json', 'w') as File:
            json.dump(self.Roles, File)

    @classmethod
    def FileCheck(self):
        if not os.path.isfile('./cogs/ownroles/roles.json'):
            with open('./cogs/ownroles/roles.json', 'w') as File:
                File.write('{}')
            return False
        return True

    @commands.command(name='createrole', pass_context=True, description='Lets you create your own role\nPLEASE WRAP ROLE NAME IN QUOTES')
    async def CreateRole(self, ctx, RoleName : str):
        if not self.IsTKSB(ctx.message.server.id):
            raise self.WrongDiscord()
        if ctx.message.author.id in self.Roles:
            if len(self.Roles[ctx.message.author.id]) >= self.MAX_CUSTOM_ROLES:
                raise self.TooManyRoles()
            else:
                CreatedRole = self.Bot.create_role(ctx.message.server, name=RoleName, hoist=True)
                self.Bot.add_roles(ctx.message.author, CreatedRole)
                self.Roles[ctx.message.author.id][CreatedRole.id] = {}
                self.SaveJson()
                MsgTD = await self.Bot.say(':white_check_mark: **Successfully created the __{}__ role and gave it to you**'.format(RoleName))
                await asyncio.sleep(5)
                await self.Bot.delete_Messages([ctx.message, MsgTD])
        else:
            CreatedRole = self.Bot.create_role(ctx.message.server, name=RoleName, hoist=True)
            self.Bot.add_roles(ctx.message.author, CreatedRole)
            self.Roles[ctx.message.author] = {}
            self.Roles[ctx.message.author][CreatedRole.id] = {}
            self.SaveJson()
            MsgTD = await self.Bot.say(':white_check_mark: **Successfully created the __{}__ role and gave it to you**'.format(RoleName))
            await asyncio.sleep(5)
            await self.Bot.delete_Messages([ctx.message, MsgTD])

    @commands.command(name='setcolor', pass_context=True, description='Lets you set the color for one of your roles\nPLEASE WRAP THE ROLE NAME IN QUOTES')
    async def SetColor(self, ctx, Color : str, RoleName : str):
        if not self.IsTKSB(ctx.message.server.id):
            raise self.WrongDiscord()
        Role = discord.utils.get(ctx.message.server.roles, name=RoleName)
        if not self.IsValidHex(Color):
            raise self.InvalidColor()
        elif Role is None:
            raise self.NonexistentRole()
        elif not Role.id in self.Roles[ctx.message.author.id]:
            raise self.UnownedRole()
        await self.Bot.edit_role(ctx.message.server, Role, color=discord.Color(hex(int(Color,16))))
        self.SaveJson()
        MsgTD = await self.Bot.say(':white_check_mark: **Successfully modified the __{}__ role\'s color to {}'.format(RoleName, Color))
        await self.Bot.delete_messages(ctx.message, MsgTD)

    @commands.command(name='editname', pass_context=True, description='Lets you change one of your role\'s names\nPLEASE WRAP ROLE NAME AND NEW NAME IN QUOTES')
    async def EditName(self, ctx, RoleName : str, NewName : str):
        if not self.IsTKSB(ctx.message.server.id):
            raise self.WrongDiscord()
        Role = discord.utils.get(ctx.message.server.roles, name=RoleName)
        if Role is None:
            raise self.NonexistentRole()
        elif not Role.id in self.Roles[ctx.message.author.id]:
            raise self.UnownedRole()
        await self.Bot.edit_role(ctx.message.server, Role, name=NewName)
        self.SaveJson()
        MsgTD = await self.Bot.say(':white_check_mark: **Successfully changed your one of your roles\' name to __{}__**'.format(NewName))
        await asyncio.sleep(5)
        await self.Bot.delete_messages([ctx.message, MsgTD])

    @commands.command(name='claimrole', pass_context=True, description='Lets you claim a role. In order to claim you must be the only person with this role\nPLEASE WRAP ROLE NAME IN QUOTES')
    async def ClaimRole(self, ctx, RoleName : str):
        if not self.IsTKSB(ctx.message.server.id):
            raise self.WrongDiscord()
        Role = discord.utils.get(ctx.message.server.roles, name=RoleName)
        if Role is None:
            raise self.NonexistentRole()
        elif not Role in ctx.message.author.roles:
            raise self.UnownedRole()
        elif ctx.message.author.id in self.Roles and Role.id in self.Roles[ctx.message.author.id]:
            raise self.AlreadyClaimed()
        Count = 0
        for Member in ctx.message.server.members:
            for role in Member.roles:
                if role.id == Role.id:
                    Count += 1
        if Count > 1:
            raise self.SharedRole()
        if not ctx.message.author.id in self.Roles:
            self.Roles[ctx.message.author.id] = {}
            self.Roles[ctx.message.author.id][Role.id] = {}
        else:
            self.Roles[ctx.message.author.id][Role.id] = {}
        self.SaveJson()
        MsgTD = await self.Bot.say(':white_check_mark: **You have successfully claimed the __{}__ role**'.format(Role.name))
        await asyncio.sleep(5)
        await self.Bot.delete_messages([ctx.message, MsgTD])

    @commands.command(name='claimedroles', pass_context=True)
    async def ClaimedRoles(self, ctx):
        if not self.IsTKSB(ctx.message.server.id):
            raise self.WrongDiscord()
        Out = '```markdown\nClaimed Roles\n=============\n'
        if not ctx.message.author.id in self.Roles or len(self.Roles[ctx.message.author.id]) == 0:
            Out += 'None'
        else:
            Roles = []
            for Role in ctx.message.author.roles:
                for ID in self.Roles[ctx.message.author.id].keys():
                    if Role.id == ID:
                        Roles.append(Role.name)
            for Role in Roles:
                Out += Role + '\n'
        Out += '```'
        await self.Bot.say(Out)

    @commands.command(name='settop', pass_context=True, description='Lets you set a role as your top role (the role that is displayed on the sidebar)\nPLEASE WRAP THE ROLE NAME IN QUOTES')
    async def SetTop(self, ctx, RoleName : str):
        if not self.IsTKSB(ctx.message.server.id):
            raise self.WrongDiscord()
        Role = discord.utils.get(ctx.message.server.roles, name=RoleName)
        if Role is None:
            raise self.NonexistentRole()
        elif not Role.id in self.Roles[ctx.message.author.id]:
            raise self.UnownedRole()
        elif Role == ctx.message.author.top_role:
            raise self.AlreadyTop()
        await self.Bot.edit_role(ctx.message.server, Role, position=ctx.message.author.top_role.position + 1)
        MsgTD = await self.Bot.say(':white_check_mark: **Successfully set __{}__ to be your top role'.format(Role.name))
        await asyncio.sleep(5)
        await self.Bot.delete_messages([message, MsgTD])

    async def on_command_error(self, Error, ctx):
        if isinstance(Error, self.WrongDiscord):
            MsgTD = await self.Bot.send_message(ctx.message.channel, ':x: **This command can only be executed in the Taco\'s Kitchen Sink Bot discord**')
            await asyncio.sleep(5)
            await self.Bot.delete_messages([ctx.message, MsgTD])
        elif isinstance(Error, self.SharedRole):
            MsgTD = await self.Bot.send_message(ctx.message.channel, ':x: **You share this role with at least one other person so you cannot claim it**')
            await asyncio.sleep(5)
            await self.Bot.delete_messages([ctx.message, MsgTD])
        elif isinstance(Error, self.TooManyRoles):
            MsgTD = await self.Bot.send_message(ctx.message.channel, ':x: **You have too many roles to create or claim more**')
            await asyncio.sleep(5)
            await self.Bot.delete_messages([ctx.message, MsgTD])
        elif isinstance(Error, self.UnownedRole):
            MsgTD = await self.Bot.send_message(ctx.message.channel, ':x: **You do not have the role you\'re trying to claim or the role you\'re trying to modify**')
            await asyncio.sleep(5)
            await self.Bot.delete_messages([ctx.message, MsgTD])
        elif isinstance(Error, self.InvalidColor):
            MsgTD = await self.Bot.send_message(ctx.message.channel, ':x: **The hex color you entered is invalid**')
            await asyncio.sleep(5)
            await self.Bot.delete_messages([ctx.message, MsgTD])
        elif isinstance(Error, self.AlreadyClaimed):
            MsgTD = await self.Bot.send_message(ctx.message.channel, ':x: **You already have this role claimed**')
            await asyncio.sleep(5)
            await self.Bot.delete_messages([ctx.message, MsgTD])
        elif isinstance(Error, self.AlreadyTop):
            MsgTD = await self.Bot.send_message(ctx.message.channel, ':x: **This role is already your top role**')
            await asyncio.sleep(5)
            await self.Bot.delete_messages([ctx.message, MsgTD])
        elif isinstance(Error, self.NonexistentRole):
            MsgTD = await self.Bot.send_message(ctx.message.channel, ':x: **The role you\'re trying to claim or modify does not exist**')
            await asyncio.sleep(5)
            await self.Bot.delete_messages([ctx.message, MsgTD])

def setup(Bot):
    Bot.add_cog(CreateOwnRoles(Bot))