import asyncio
import os
import discord
from discord.ext import commands


class Moderation:

    ILBSERVERS = None

    BANNEDNAMES = [
        'discord.gg',
        'add me',
        'add pls',
        'pls add',
        'hello drunk weathers'
    ]

    PE = {}

    def __init__(self, Bot):
        self.Bot = Bot
        self.ILBInit()

    @classmethod
    def GetPerms(self, ctx):
        if not isinstance(ctx.message.author, discord.Member):
            return False
        return ctx.message.author.server_permissions

    @classmethod
    def ILBInit(self):
        self.ILBSERVERS = []
        if not os.path.isfile('./cogs/cfg/ilb.txt'):
            open('./cogs/cfg/ilb.txt', 'a').close()
        else:
            with open('./cogs/cfg/ilb.txt', 'r') as File:
                for Line in File:
                    if Line != '' and Line != '\n' and not Line.isspace():
                        self.ILBSERVERS.append(Line.strip())

    @classmethod
    def ILBEnabled(self, Member):
        return Member.server.id in self.ILBSERVERS

    @commands.command(name='hackban', pass_context=True, hidden=True, aliases=['hb'])
    async def Hackban(self, ctx, ID : int):
        Perms = self.GetPerms(ctx)
        if Perms == False:
            return
        if not Perms.ban_members and not Perms.administrator:
            MsgTD = await self.Bot.say(":x: **You don't have the required permissions to execute this command.**")
            await asyncio.sleep(5)
            await self.Bot.delete_messages([ctx.message, MsgTD])
        else:
            try:
                Member = discord.Object(id=ID)
                Member.server = ctx.message.server
                UserInfo = await self.Bot.get_user_info(ID)
            except discord.NotFound:
                MsgTD = await self.Bot.say(":x: **User with ID {} doesn't exist.**".format(ID))
                await asyncio.sleep(5)
                await self.Bot.delete_messages([ctx.message, MsgTD])
            except discord.HTTPException:
                MsgTD = await self.Bot.say(":x: **Unknown error occured.**")
                await asyncio.sleep(5)
                await self.Bot.delete_messages([ctx.message, MsgTD])
            await self.Bot.ban(Member)
            MsgTD = await self.Bot.say(":white_check_mark: **Banned user __{}__ with ID __{}__.**".format(UserInfo.name, UserInfo.id))
            await asyncio.sleep(5)
            await self.Bot.delete_messages([ctx.message, MsgTD])

    @commands.command(name='deleteall', pass_context=True, hidden=True)
    async def DeleteAll(self, ctx, ID : int):
        Perms = self.GetPerms(ctx)
        if Perms == False:
            return
        if Perms.manage_messages:
            BotPerms = ctx.message.server.get_member(self.Bot.user.id).server_permissions
            if not BotPerms.manage_messages:
                await self.Bot.say(':x: **I don\'t have permission to delete messages :c**')
                return
            MsgCount = 0
            for Channel in ctx.message.server.channels:
                async for Message in self.Bot.logs_from(Channel, limit=500):
                    if Message.author.id == str(ID):
                        await self.Bot.delete_message(Message)
                        MsgCount += 1
                    continue

    @commands.command(name='newrole', pass_context=True)
    async def NewRole(self, ctx, Role : str):
        Perms = self.GetPerms(ctx)
        if Perms is False:
            return
        if Perms.manage_roles:
            BotPerms = ctx.message.server.get_member(self.Bot.user.id).server_permissions
            if not BotPerms.manage_roles:
                await self.Bot.say(':x: **I don\'t have permission to manage roles :c**')
                return
            Roles = ctx.message.content.split(' ', 1)[1].split('\n')
            Embed = discord.Embed(title="New Roles")
            for RoleName in Roles:
                await self.Bot.create_role(ctx.message.server, name=RoleName)
            Embed.add_field(name='Created Roles', value=', '.join(Roles))
            await self.Bot.say(embed=Embed)

    @commands.command(name='permissioneditor', pass_context=True, aliases=['permissionseditor', 'pe'])
    async def PermissionsEditor(self, ctx):
        if ctx.message.author.id not in self.PE:
            self.PE[ctx.message.author.id] = {
                'SelectedRoles' : {},
                'PermSettings' : {}
            }

    @commands.command(name='invitelinkbanner', pass_context=True, aliases=['ilb'])
    async def ILB(self, ctx):
        Perms = self.GetPerms(ctx)
        if Perms is False:
            MsgTD = await self.Bot.say(':x: **Couldn\'t get perms for you')
            await asyncio.sleep(5)
            await self.Bot.delete_messages([ctx.message, MsgTD])
            return
        if Perms.administrator or (Perms.manage_server and Perms.ban_members):
            ILBFile = open('./cogs/cfg/ilb.txt', 'r+')
            Servers = [Line for Line in ILBFile.readlines()]
            for Server in Servers:
                if Server == '' or Server.isspace():
                    Servers.remove(Server)
            ILBFile.close()
            Remove = ctx.message.server.id in Servers
            Servers.remove(ctx.message.server.id) if ctx.message.server.id in Servers else Servers.append(ctx.message.server.id)
            i = 0
            ILBFile = open('./cogs/cfg/ilb.txt', 'w')
            while i < len(Servers):
                ILBFile.write('\n' + Servers[i]) if i > 0 else ILBFile.write(Servers[i])
                i += 1
            ILBFile.flush()
            ILBFile.close()
            MsgTD = await self.Bot.say(':white_check_mark: **Successfully disabled invite link instabanning for this server.**' if Remove else ':white_check_mark: **Successfully enabled invite link instabanning for this server.**')
            await asyncio.sleep(5)
            await self.Bot.delete_messages([ctx.message, MsgTD])
            self.ILBInit()
        else:
            MsgTD = await self.Bot.say(":x: **You don't have the required permissions to execute this command.**")
            await asyncio.sleep(5)
            await self.Bot.delete_messages([ctx.message, MsgTD])

    @commands.command(name='ilbenabled', pass_context=True)
    async def ILBEnabledMethod(self, ctx):
        await self.Bot.say(':white_check_mark: **Invite link instabanning is enabled here**' if self.ILBEnabled(ctx.message.author) else ':x: **Invite link instabanning is disabled here**')

    async def on_member_join(self, Member):
        if self.ILBEnabled(Member) and any(Name in Member.name.lower() for Name in self.BANNEDNAMES):
            print('Banned {} from {}'.format(Member.name, Member.server.name))
            await self.Bot.ban(Member)


def setup(Bot):
    Bot.add_cog(Moderation(Bot))