import re
import discord
from discord.ext import commands


class Conversion:
    def __init__(self, Bot):
        self.Bot = Bot

    @classmethod
    def text_to_bits(self, text, encoding='utf-8', errors='surrogatepass'):
        bits = bin(int.from_bytes(text.encode(encoding, errors), 'big'))[2:]
        return bits.zfill(8 * ((len(bits) + 7) // 8))

    @classmethod
    def text_from_bits(self, bits, encoding='utf-8', errors='surrogatepass'):
        n = int(bits, 2)
        return n.to_bytes((n.bit_length() + 7) // 8, 'big').decode(encoding, errors) or '\0'

    @commands.group(name='displacement', pass_context=True)
    async def Displacement(ctx):
        if ctx.invoked_subcommand is None:
            await self.Bot.say('Invalid subcommand entered')

    @Displacement.command(name='cctol', pass_context=True)
    async def CCToL(self, ctx, CC : float):
        await self.Bot.say(str(round(CC / 1000, 3)))

    @Displacement.command(name='cctoci', pass_context=True, aliases=['cctocubicinches', 'cubiccentimeterstocubicinches', 'cubiccentimeterstoci'])
    async def CCToCI(self, ctx, CC : float):
        await self.Bot.say(str(round(CC * 0.0610237441, 3)))

    @Displacement.command(name='citocc', pass_context=True, aliases=['cubicinchestocc', 'cubicinchestocubiccentimeters', 'citocubiccentimeters'])
    async def CIToCC(self, ctx, CI : float):
        await self.Bot.say(str(round(CI / 0.0610237441, 3)))

    @Displacement.command(name='ltocc', pass_context=True, aliases=['literstocc', 'ltocubiccentimeters', 'literstocubiccentimeters'])
    async def LToCC(self, ctx, L : float):
        await self.Bot.say(str(round(L * 1000, 2)))

    @commands.command(name='binary', pass_context=True, aliases=['bin'])
    async def Binary(self, ctx, StringToConvert : str):
        Embed = discord.Embed()
        Parts = ctx.message.content.split(' ', 1)
        if StringToConvert.lower().startswith('0b'):
            Embed.colour = 0xFF0000
            Embed.title = 'Binary Conversion Failed'
            Embed.description = ':x: **`{}` is already in binary.**'.format(Parts[1])
            Embed.add_field(name='Incorrect Type Detected?', value='`0x` before hexadecimals\n`0b` before binary numbers\n`0o` before octals')
            await self.Bot.say(embed=Embed)
        else:
            Embed.colour = 0x00FF00
            if StringToConvert.lower().startswith('0x'):
                Embed.add_field(name = 'Original Value (Hexadecimal)', value = Parts[1], inline = False)
                Embed.add_field(name = 'Binary Value', value = str(bin(int(Parts[1], 16))).replace('0b', ''), inline = False)
                Embed.add_field(name = 'Incorrect Type Detected?', value = '`0x` before hexadecimals\n`0b` before binary numbers\n`0o` before octals')
                await self.Bot.say(embed=Embed)
            elif StringToConvert.lower().startswith('0o'):
                Embed.add_field(name = 'Original Value (Octal)', value = Parts[1], inline = False)
                Embed.add_field(name = 'Binary Value', value = str(bin(int(Parts[1], 8))).replace('0b', ''), inline = False)
                Embed.add_field(name = 'Incorrect Type Detected?', value = '`0x` before hexadecimals\n`0b` before binary numbers\n`0o` before octals')
            else:
                try:
                    Embed.add_field(name = 'Original Value (Decimal)', value = Parts[1], inline = False)
                    Embed.add_field(name = 'Binary Value', value = '{:b}'.format(int(Parts[1])), inline = False)
                    Embed.add_field(name = 'Incorrect Type Detected?', value = '`0x` before hexadecimals\n`0b` before binary numbers\n`0o` before octals')
                    await self.Bot.say(embed=Embed)
                except ValueError:
                    Embed = discord.Embed(color = 0x00FF00)
                    Embed.add_field(name = 'Original Value (ASCII)', value = Parts[1], inline = False)
                    Embed.add_field(name = 'Binary Value', value = str(self.text_to_bits(Parts[1])).replace('0b', ''), inline = False)
                    Embed.add_field(name = 'Incorrect Type Detected?', value = '`0x` before hexadecimals\n`0b` before binary numbers\n`0o` before octals')
                    await self.Bot.say(embed=Embed)


    @commands.command(name='hexadecimal', pass_context=True, aliases=['hex'])
    async def Hexadecimal(self, ctx, StringToConvert : str):
        Embed = discord.Embed()
        Parts = ctx.message.content.split(' ', 1)
        if Parts[1].lower().startswith("0x"): # Already a hexadecimal
            Embed.colour = 0xFF0000
            Embed.title = "Hexadecimal Conversion Failed"
            Embed.description = ":x: **`{}` is already a hexadecimal.**".format(Parts[1])
            Embed.add_field(name = "Incorrect Type Detected?", value = "`0x` before hexadecimals\n`0b` before binary numbers\n`0o` before octals")
            await self.Bot.say(embed=Embed)
        else:
            Embed.colour = 0x00FF00
            if Parts[1].lower().startswith("0b"): # Binary
                Embed.add_field(name = "Original Value (Binary)", value = Parts[1].replace("0b", ""), inline = False)
                Embed.add_field(name = "Hexadecimal Value", value = str(hex(int(Parts[1], 2))).replace("0x", "").upper(), inline = False)
                Embed.add_field(name = "Incorrect Type Detected?", value = "`0x` before hexadecimals\n`0b` before binary numbers\n`0o` before octals")
                await self.Bot.say(embed=Embed)
            elif Parts[1].lower().startswith("0o"): # Octal
                Embed.add_field(name = "Original Value (Octal)", value = Parts[1].replace("0o", ""), inline = False)
                Embed.add_field(name = "Hexadecimal Value", value = str(hex(int(Parts[1], 8))).replace("0x", "").upper(), inline = False)
                Embed.add_field(name = "Incorrect Type Detected?", value = "`0x` before hexadecimals\n`0b` before binary numbers\n`0o` before octals")
                await self.Bot.say(embed=Embed)
            else:
                try:
                    Embed.add_field(name = "Original Value (Decimal)", value = Parts[1], inline = False)
                    Embed.add_field(name = "Hexadecimal Value", value = "{:x}".format(int(Parts[1])).upper(), inline = False)
                    Embed.add_field(name = "Incorrect Type Detected?", value = "`0x` before hexadecimals\n`0b` before binary numbers\n`0o` before octals")
                    await self.Bot.say(embed=Embed)
                except ValueError:
                    Embed = discord.Embed(color = 0x00FF00)
                    Embed.add_field(name = "Original Value (ASCII)", value = Parts[1], inline = False)
                    Embed.add_field(name = "Hexadecimal Value", value = str(hex(int(self.text_to_bits(Parts[1]), 2))).replace("0x", "").upper(), inline = False)
                    Embed.add_field(name = "Incorrect Type Detected?", value = "`0x` before hexadecimals\n`0b` before binary numbers\n`0o` before octals")
                    await self.Bot.say(embed=Embed)


    @commands.command(name='octal', pass_context=True, aliases=['oct'])
    async def Octal(self, ctx, StringToConvert : str):
        Embed = discord.Embed()
        Parts = ctx.message.content.split(' ', 1)
        if Parts[1].lower().startswith("0o"): # Already an octal
            Embed.colour = 0xFF0000
            Embed.title = "Octal Conversion Failed"
            Embed.description = ":x: **`{}` is already an octal.**".format(Parts[1])
            Embed.add_field(name = "Incorrect Type Detected?", value = "`0x` before hexadecimals\n`0b` before binary numbers\n`0o` before octals")
            await self.Bot.say(embed=Embed)
        Embed.colour = 0x00FF00
        if Parts[1].lower().startswith("0b"): # Binary
            Embed.add_field(name = "Original Value (Binary)", value = Parts[1].replace("0b", ""), inline = False)
            Embed.add_field(name = "Octal Value", value = str(oct(int(Parts[1], 2))).replace("0o", "").upper(), inline = False)
            Embed.add_field(name = "Incorrect Type Detected?", value = "`0x` before hexadecimals\n`0b` before binary numbers")
            await self.Bot.say(embed=Embed)
        elif Parts[1].lower().startswith("0x"): # Hexadecimal
            Embed.add_field(name = "Original Value (Hexadecimal)", value = Parts[1].replace("0x", ""), inline = False)
            Embed.add_field(name = "Octal Value", value = str(oct(int(Parts[1], 16))).replace("0o", "").upper(), inline = False)
            Embed.add_field(name = "Incorrect Type Detected?", value = "`0x` before hexadecimals\n`0b` before binary numbers")
            await self.Bot.say(embed=Embed)
        else:
            try:
                Embed.add_field(name = "Original Value (Decimal)", value = Parts[1], inline = False)
                Embed.add_field(name = "Octal Value", value = "{:o}".format(int(Parts[1])).upper(), inline = False)
                Embed.add_field(name = "Incorrect Type Detected?", value = "`0x` before hexadecimals\n`0b` before binary numbers\n`0o` before octals")
                await self.Bot.say(embed=Embed)
            except ValueError:
                Embed = discord.Embed(color = 0x00FF00)
                Embed.add_field(name = "Original Value (ASCII)", value = Parts[1], inline = False)
                Embed.add_field(name = "Octal Value", value = str(oct(int(self.text_to_bits(Parts[1]), 2))).replace("0o", "").upper(), inline = False)
                Embed.add_field(name = "Incorrect Type Detected?", value = "`0x` before hexadecimals\n`0b` before binary numbers\n`0o` before octals")
                await self.Bot.say(embed=Embed)


    @commands.command(name='ascii', pass_context=True)
    async def ASCII(self, ctx, StringToConvert : str):
        Embed = discord.Embed(color = 0x00FF00)
        Parts = ctx.message.content.split(' ', 1)
        if Parts[1].lower().startswith("0b"): # Binary
            Embed.add_field(name = "Original Value (Binary)", value = Parts[1].replace("0b", ""), inline = False)
            Embed.add_field(name = "ASCII Value", value = self.text_from_bits(Parts[1]), inline = False)
            Embed.add_field(name = "Incorrect Type Detected?", value = "`0x` before hexadecimals\n`0b` before binary numbers\n`0o` before octals")
            await self.Bot.say(embed=Embed)
        elif Parts[1].lower().startswith("0x"): # Hexadecimal
            Embed.add_field(name = "Original Value (Hexadecimal)", value = Parts[1].replace("0x", ""), inline = False)
            Embed.add_field(name = "ASCII Value", value = self.text_from_bits(bin(int(Parts[1], 16))), inline = False)
            Embed.add_field(name = "Incorrect Type Detected?", value = "`0x` before hexadecimals\n`0b` before binary numbers\n`0o` before octals")
            await self.Bot.say(embed=Embed)
        elif Parts[1].lower().startswith("0o"): # Octal
            Embed.add_field(name = "Original Value (Octal)", value = Parts[1].replace("0x", ""), inline = False)
            Embed.add_field(name = "ASCII Value", value = self.text_from_bits(bin(int(Parts[1], 8))), inline = False)
            Embed.add_field(name = "Incorrect Type Detected?", value = "`0x` before hexadecimals\n`0b` before binary numbers\n`0o` before octals")
            await self.Bot.say(embed=Embed)
        elif re.search('[a-zA-Z]', Parts[1]) is not None:
            Embed.colour = 0xFF0000
            Embed.add_field(name = "ASCII Conversion Failed", value = ":x: **`{}` is already in ASCII.**\n\n".format(Parts[1]))
            Embed.add_field(name = "Incorrect Type Detected?", value = "`0x` before hexadecimals\n`0b` before binary numbers\n`0o` before octals")
            await self.Bot.say(embed=Embed)
        else: # Decimal
            Embed.add_field(name = "Original Value (Decimal)", value = Parts[1].replace("0x", ""), inline = False)
            Embed.add_field(name = "ASCII Value", value = self.text_from_bits(bin(int(Parts[1]))), inline = False)
            Embed.add_field(name = "Incorrect Type Detected?", value = "`0x` before hexadecimals\n`0b` before binary numbers\n`0o` before octals")
            await self.Bot.say(embed=Embed)


    @commands.command(name='decimal', pass_context=True, aliases=['dec'])
    async def Decimal(self, ctx, StringToConvert : str):
        Embed = discord.Embed()
        Parts = ctx.message.content.split(' ', 1)
        if Parts[1].lower().startswith("0b"): # Binary
            Embed.colour = 0x00FF00
            Embed.add_field(name = "Original Value (Binary)", value = Parts[1].replace("0b", ""), inline = False)
            Embed.add_field(name = "Decimal Value", value = int(Parts[1]), inline = False)
            Embed.add_field(name = "Incorrect Type Detected?", value = "`0x` before hexadecimals\n`0b` before binary numbers\n`0o` before octals")
            await self.Bot.say(embed=Embed)
        elif Parts[1].lower().startswith("0x"): # Hexadecimal
            Embed.colour = 0x00FF00
            Embed.add_field(name = "Original Value (Hexadecimal)", value = Parts[1].replace("0x", ""), inline = False)
            Embed.add_field(name = "Decimal Value", value = int(Parts[1], 16), inline = False)
            Embed.add_field(name = "Incorrect Type Detected?", value = "`0x` before hexadecimals\n`0b` before binary numbers\n`0o` before octals")
            await self.Bot.say(embed=Embed)
        elif Parts[1].lower().startswith("0o"): # Octal
            Embed.colour = 0x00FF00
            Embed.add_field(name = "Original Value (Octal)", value = Parts[1].replace("0x", ""), inline = False)
            Embed.add_field(name = "Decimal Value", value = int(Parts[1], 8), inline = False)
            Embed.add_field(name = "Incorrect Type Detected?", value = "`0x` before hexadecimals\n`0b` before binary numbers\n`0o` before octals")
            await self.Bot.say(embed=Embed)
        else:
            try:
                int(Parts[1])
                Embed.colour = 0xFF0000
                Embed.title = "Decimal Conversion Failed"
                Embed.description = "**`{}` is already a decimal.**".format(Parts[1])
                Embed.add_field(name = "Incorrect Type Detected?", value = "`0x` before hexadecimals\n`0b` before binary numbers\n`0o` before octals")
                await self.Bot.say(embed=Embed)
            except ValueError: # ASCII
                Embed.colour = 0x00FF00
                Embed.add_field(name = "Original Value (ASCII)", value = Parts[1], inline = False)
                Embed.add_field(name = "Decimal Value", value = int(self.text_to_bits(Parts[1]), 2), inline = False)
                Embed.add_field(name = "Incorrect Type Detected?", value = "`0x` before hexadecimals\n`0b` before binary numbers\n`0o` before octals")
                await self.Bot.say(embed=Embed)


def setup(Bot):
    Bot.add_cog(Conversion(Bot))