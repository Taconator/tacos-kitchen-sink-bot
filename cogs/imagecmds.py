import random
import time
from types import SimpleNamespace
import discord
from discord.ext import commands
from utils import checks


class ImageCMDs:
    ImagesDict = {}
    Images = None

    SecureRandom = random.SystemRandom()

    @commands.command(name='images', pass_context=True, aliases=['imgs'])
    async def ImagesMethod(self, ctx):
        Out = '```markdown\nImages\n======\n\n'
        i = 1
        for Key, _ in self.Images.__dict__.items():
            Out += '{}.\t{}\n'.format(i, Key)
            i += 1
        Out += '```'
        await self.Bot.say(Out)

    def __init__(self, Bot):
        self.Bot = Bot

    async def LoadImages(self):
        Start = time.time()
        self.ImagesDict = {}
        async for Message in self.Bot.logs_from(self.Bot.get_channel('472883797692776458'), limit=500):
            Names = Message.content.split(',')
            URL = Message.attachments[0]['url']
            Dict = {}
            for Name in Names:
                Dict[Name] = URL
            self.ImagesDict.update(Dict)
        self.Images = SimpleNamespace(**self.ImagesDict)
        End = time.time()
        return End - Start

    @commands.command(name='image', pass_context=True, aliases=['im', 'img'])
    async def Image(self, ctx):
        Parts = ctx.message.content.split(' ', 1)
        Parts[1] = Parts[1].replace(':', '').lower()
        if len(Parts) < 2:
            await self.Bot.say(':x: **No image entered**')
            return
        Parts[1] = Parts[1].replace(':', '')
        if not Parts[1].lower() in self.Images.__dict__:
            await self.Bot.say(':x: **Invalid image entered. Do `^images` to see what images are available.**')
            return
        Embed = discord.Embed(color=int("%06x" % random.randint(0, 0xFFFFFF), 16), title=Parts[1])
        Embed.set_image(url=self.Images.__dict__[Parts[1]])
        Embed.set_author(name=ctx.message.author.display_name)
        await self.Bot.say(embed=Embed)
        await self.Bot.delete_message(ctx.message)

    async def on_ready(self):
        Time = await self.LoadImages()
        print('Loaded all ^im images in {} seconds'.format(round(Time, 2)))

    @commands.command(name='imreload', pass_context=True)
    @checks.IsOwner()
    async def ImageReload(self, ctx):
        MsgTE = await self.Bot.say('**Please wait, reloading ^im images**')
        Time = await self.LoadImages()
        await self.Bot.edit_message(MsgTE, new_content=':white_check_mark: **Done reloading {} ^im images in {} seconds**'.format(len(self.ImagesDict), str(round(Time, 2))))
        await self.Bot.delete_message(ctx.message)


def setup(Bot):
    Bot.add_cog(ImageCMDs(Bot))