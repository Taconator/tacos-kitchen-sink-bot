import asyncio
import discord
from discord.ext import commands


class TTFSRoles:
    def __init__(self, Bot):
        self.Bot = Bot

    @classmethod
    def IsTTFS(self, ServerID):
        return ServerID == '361553346580185089'

    @commands.command(name='role', pass_context=True)
    async def TTFSRole(self, ctx, RoleName : str):
        if not self.IsTTFS(ctx.message.server.id):
            return
        if RoleName.lower() == 'degenerates':
            RARole = discord.utils.get(ctx.message.server.roles, name='Retard Alert')
            DegeneratesRole = discord.utils.get(ctx.message.server.roles, name='degenerates')
            if RARole in [role for role in ctx.message.author.roles]:
                MsgTD = await self.Bot.say('**No**')
                await asyncio.sleep(5)
                await self.Bot.delete_messages([ctx.message, MsgTD])
                return
            if DegeneratesRole in [role for role in ctx.message.author.roles]:
                await self.Bot.remove_roles(ctx.message.author, DegeneratesRole)
                MsgTD = await self.Bot.say(':white_check_mark: **Successfully took the `degenerates` role away from you.**')
                await asyncio.sleep(5)
                await self.Bot.delete_messages([ctx.message, MsgTD])
            else:
                await self.Bot.add_roles(ctx.message.author, DegeneratesRole)
                MsgTD = await self.Bot.say(':white_check_mark: **Successfully gave you the `degenerates` role.**')
                await asyncio.sleep(5)
                await self.Bot.delete_messages([ctx.message, MsgTD])
        elif RoleName.lower() == 'megagay':
            MegaGayRole = discord.utils.get(ctx.message.server.roles, name='Thot Squad')
            if MegaGayRole in [role for role in ctx.message.author.roles]:
                await self.Bot.remove_roles(ctx.message.author, MegaGayRole)
                MsgTD = await self.Bot.say(':white_check_mark: **Successfully took the `Thot Squad` role away from you.**')
                await asyncio.sleep(5)
                await self.Bot.delete_messages([ctx.message, MsgTD])
            else:
                await self.Bot.add_roles(ctx.message.author, MegaGayRole)
                MsgTD = await self.Bot.say(':white_check_mark: **Congratulations you are now a thot**')
                await asyncio.sleep(5)
                await self.Bot.delete_messages([ctx.message, MsgTD])
        else:
            await self.Bot.say(':x: **Invalid role specified.**')


def setup(Bot):
    Bot.add_cog(TTFSRoles(Bot))