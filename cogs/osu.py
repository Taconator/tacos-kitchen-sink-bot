import asyncio
import osuapi
from osuapi import OsuApi, ReqConnector
import discord
from discord.ext import commands


class Osu:
    API = OsuApi('Nice Try', connector=ReqConnector())
    Users = {}

    def __init__(self, Bot):
        self.Bot = Bot

    @commands.command(name='osu', pass_context=True)
    async def OsuMethod(self, ctx):
        Embed = discord.Embed(color=0xE984B2, title='Osu! Lookup', description='Hi, {}! Please enter the number corresponding to the osu! gamemode you would like to view'.format(ctx.message.author.display_name))
        Embed.set_footer(text='Orrrrr, you can type exit to leave the Osu! Lookup menu')
        Embed.add_field(name='1', value='osu!')
        Embed.add_field(name='2', value='Taiko')
        Embed.add_field(name='3', value='CatchTheBeat')
        Embed.add_field(name='4', value='osu!Mania')
        Message = await self.Bot.say(embed=Embed)
        self.Users[ctx.message.author.id] = {'Phase' : 0, 'Message' : Message}

    async def on_message(self, Message):
        if not Message.author.id in self.Users:
            return
        if Message.content.lower() == 'exit':
            await self.Bot.delete_message(self.Users[Message.author.id]['Message'])
            del self.Users[Message.author.id]
            MsgTD = await self.Bot.send_message(Message.channel, ':white_check_mark: **Removed {} from the Osu! Lookup menu.**'.format(Message.author.display_name))
            await asyncio.sleep(5)
            await self.Bot.delete_messages([MsgTD, Message])
            return
        if self.Users[Message.author.id]['Phase'] == 0:
            try:
                MessageInt = int(Message.content.lower())
            except Exception:
                await self.Bot.send_message(Message.channel, ':x: **Invalid option entered.**')
                return
            if MessageInt > 0 and MessageInt < 5:
                self.Users[Message.author.id]['Phase'] = 1
                if MessageInt == 1:
                    self.Users[Message.author.id]['Gamemode'] = osuapi.OsuMode.osu
                elif MessageInt == 2:
                    self.Users[Message.author.id]['Gamemode'] = osuapi.OsuMode.taiko
                elif MessageInt == 3:
                    self.Users[Message.author.id]['Gamemode'] = osuapi.OsuMode.ctb
                else:
                    self.Users[Message.author.id]['Gamemode'] = osuapi.OsuMode.mania
                Embed = discord.Embed(color=0xE984B2, title='Osu! Lookup', description='Ok, now select what you want to look up')
                Embed.set_footer(text='Orrrrr, you can type exit to leave the Osu! Lookup menu')
                Embed.add_field(name='1', value='Signature')
                Embed.add_field(name='2', value='User Profile')
                Embed.add_field(name='3', value='Best Plays')
                Embed.add_field(name='4', value='Recent Plays')
                await self.Bot.edit_message(self.Users[Message.author.id]['Message'], embed=Embed)
                await self.Bot.delete_message(Message)
            else:
                await self.Bot.send_Message(Message.channel, ':x: **Invalid option entered.**')
                return
        elif self.Users[Message.author.id]['Phase'] == 1:
            try:
                MessageInt = int(Message.content.lower())
            except Exception:
                await self.Bot.send_Message(Message.channel, ':x: **Invalid option entered.**')
                return
            if MessageInt > 0 and MessageInt < 5:
                self.Users[Message.author.id]['Phase'] = 2
                self.Users[Message.author.id]['Lookup'] = MessageInt
                await self.Bot.edit_message(self.Users[Message.author.id]['Message'], embed=discord.Embed(title='Finally, type a username for me to lookup', color=0xE984B2, description='Orrrrr, type `exit` to exit Osu! Lookup menu'))
                await self.Bot.delete_message(Message)
            else:
                await self.Bot.send_Message(Message.channel, ':x: **Invalid option entered.**')
                return
        elif self.Users[Message.author.id]['Phase'] == 2:
            if self.Users[Message.author.id]['Lookup'] == 1:
                Result = self.API.get_user(username=Message.content, mode=self.Users[Message.author.id]['Gamemode'])
                if len(Result) == 0:
                    await self.Bot.say(':x: **User not found. Try again or type `exit` to cancel.**')
                    return
                Result = Result[0]
                Embed = discord.Embed(color=0xE984B2, title=Message.content, description='Signature')
                Embed.add_field(name='PP', value=str(int(Result.pp_raw)))
                Embed.add_field(name='Global Ranking', value=str(Result.pp_rank))
                Embed.add_field(name='Country', value=Result.country)
                Embed.add_field(name='Accuracy', value=str(round(Result.accuracy, 2)) + '%')
                Embed.add_field(name='Play Count', value=str(Result.playcount))
                Embed.add_field(name='Level', value=str(int(Result.level)))
                await self.Bot.edit_message(self.Users[Message.author.id]['Message'], embed=Embed)
                await self.Bot.delete_message(Message)
                del self.Users[Message.author.id]
                return
            if self.Users[Message.author.id]['Lookup'] == 2:
                Result = self.API.get_user(username=Message.content, mode=self.Users[Message.author.id]['Gamemode'])
                if len(Result) == 0:
                    await self.Bot.say(':x: **User not found. Try again or type `exit` to cancel.**')
                    return
                Result = Result[0]
                Embed = discord.Embed(color=0xE984B2, title=Message.content, description='User Profile')
                Embed.add_field(name='Country', value=Result.country)
                Embed.add_field(name='Play Count', value=Result.playcount)
                Embed.add_field(name='Level', value=str(int(Result.level)))
                Embed.add_field(name='Ranked Score', value=str(Result.ranked_score))
                Embed.add_field(name='Total Score', value=str(Result.total_score))
                Embed.add_field(name='PP', value=str(int(Result.pp_raw)))
                Embed.add_field(name='Rank', value='{} (Country Rank: {})'.format(str(Result.pp_rank), str(Result.pp_country_rank)))
                Embed.add_field(name='Accuracy', value=str(round(Result.accuracy)) + '%')
                Embed.add_field(name='300s', value=str(Result.count300))
                Embed.add_field(name='100s', value=str(Result.count100))
                Embed.add_field(name='50s', value=str(Result.count50))
                Embed.add_field(name='SS', value=str(Result.count_rank_ss))
                Embed.add_field(name='S', value=str(Result.count_rank_s))
                Embed.add_field(name='A', value=str(Result.count_rank_a))
                Embed.set_footer(text=str(self.Users[Message.author.id]['Gamemode']))
                await self.Bot.edit_message(self.Users[Message.author.id]['Message'], embed=Embed)
                await self.Bot.delete_message(Message)
                del self.Users[Message.author.id]
                return
            if self.Users[Message.author.id]['Lookup'] == 3:
                Results = self.API.get_user_best(username=Message.content, mode=self.Users[Message.author.id]['Gamemode'], limit=5)
                BeatmapResults = []
                if len(Results) == 0:
                    await self.Bot.send_message(Message.channel, ':x: **User not found. Try again or type `exit` to cancel.**')
                    return
                for Score in Results:
                    BeatmapResults.append(self.API.get_beatmaps(beatmap_id=Score.beatmap_id))
                Embed = discord.Embed(color=0xE984B2, title=Message.content, description='Best Scores')
                i = 0
                while i < len(BeatmapResults):
                    Embed.add_field(name='{}. {} (★{})'.format(str(i + 1), BeatmapResults[i][0].title, str(round(BeatmapResults[i][0].difficultyrating, 2))), value='**PP:** {}\n**Rank:** {}\n**Max Combo:** {}\n**Score:** {}\n**Date:** {}'.format(str(int(Results[i].pp)), Results[i].rank, str(Results[i].maxcombo), str(Results[i].score), str(Results[i].date)))
                    i += 1
                Embed.set_footer(text=str(self.Users[Message.author.id]['Gamemode']))
                await self.Bot.edit_message(self.Users[Message.author.id]['Message'], embed=Embed)
                await self.Bot.delete_message(Message)
                del self.Users[Message.author.id]
                return
            if self.Users[Message.author.id]['Lookup'] == 4:
                Results = self.API.get_user_recent(username=Message.content, mode=self.Users[Message.author.id]['Gamemode'], limit=5)
                BeatmapResults = []
                if len(Results) == 0:
                    await self.Bot.send_message(Message.channel, ':x: **User has not played osu recently or user not found.**')
                    await self.Bot.delete_messages([self.Users[Message.author.id]['Message'], Message])
                    del self.Users[Message.author.id]
                    return
                for Score in Results:
                    BeatmapResults.append(self.API.get_beatmaps(beatmap_id=Score.beatmap_id))
                Embed = discord.Embed(color=0xE984B2, title=Message.content, description='Recent Scores')
                i = 0
                while i < len(BeatmapResults):
                    Embed.add_field(name='{}. {} (★{})'.format(str(i + 1), BeatmapResults[i][0].title, str(round(BeatmapResults[i][0].difficultyrating, 2))), value='**PP:** {}\n**Rank:** {}\n**Max Combo:** {}\n**Score:** {}\n**Date:** {}'.format(str(int(Results[i].pp)), Results[i].rank, str(Results[i].maxcombo), str(Results[i].score), str(Results[i].date)))
                    i += 1
                Embed.set_footer(text=str(self.Users[Message.author.id]['Gamemode']))
                await self.Bot.edit_message(self.Users[Message.author.id]['Message'], embed=Embed)
                await self.Bot.delete_message(Message)
                del self.Users[Message.author.id]
                return


def setup(Bot):
    Bot.add_cog(Osu(Bot))