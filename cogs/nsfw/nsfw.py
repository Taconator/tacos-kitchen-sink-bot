import asyncio
from pathlib import Path
import discord
from discord.ext import commands
from pybooru import Moebooru

Konachan = Moebooru('konachan')
KonaLimit = 100


class Search:
    def __init__(self, Message, Author, Tags):
        self.Message = Message if isinstance(Message, discord.Message) else ""
        self.Author = Author if Author is not None and Author != "" else ""
        self.Tags = Tags if Tags is not None and Tags != "" else ""
        self.Image = 1

    def ExecuteSearch(self):
        self.Results = Konachan.post_list(tags=self.Tags, limit=KonaLimit, page=self.Image)


class NSFW:
    SEARCHMSGS = {}
    ALLOWEDCHANNELS = [
        "467825686174760970",
        "233014325890121728",
        "468102671341977610",
        "468301806519189504"
    ]

    BLACKLISTED = [
          'loli',
          'furry'
    ]
    WaitingForPage = []

    def __init__(self, Bot):
        self.Bot = Bot
        self.ReadChannels()

    def ReadChannels(self):
        self.ALLOWEDCHANNELS = []
        Channels = Path('./cogs/nsfw/channels.txt')
        if not Channels.is_file():
            open('./cogs/nsfw/channels.txt', 'a').close()
        else:
            with open('./cogs/nsfw/channels.txt', 'r') as File:
                for Line in File:
                    self.ALLOWEDCHANNELS.append(Line.strip())

    def UserHaveSearch(self, ID):
        return ID in self.SEARCHMSGS

    def MsgASearch(self, Message):
        if not self.UserHaveSearch(Message.author.id):
            return False
        return self.SEARCHMSGS[Message.author.id] == Message

    async def WaitForResponse(self, ctx, SearchMsg):
        Response = await self.Bot.wait_for_reaction(emoji=["\u2B05", "\u27A1", "\u23F9", "\U0001F522"], user=ctx.message.author, message=SearchMsg.Message)
        await self.HandleResponse(ctx, SearchMsg, Response)

    async def HandleResponse(self, ctx, SearchMsg, Response):
        if Response.reaction.emoji == '\u23F9':
            await self.Bot.clear_reactions(SearchMsg.Message)
            del self.SEARCHMSGS[ctx.message.author.id]
        elif Response.reaction.emoji == '\u27A1':
            await self.NextImage(SearchMsg)
            await self.Bot.remove_reaction(SearchMsg.Message, '\u27A1', ctx.message.author)
            await self.WaitForResponse(ctx, SearchMsg)
        elif Response.reaction.emoji == '\u2B05':
            await self.PrevImage(SearchMsg)
            await self.Bot.remove_reaction(SearchMsg.Message, '\u2B05', ctx.message.author)
            await self.WaitForResponse(ctx, SearchMsg)
        elif Response.reaction.emoji == '\U0001F522':
            self.WaitingForPage.append(ctx.message.author.id)
            await self.Bot.say('**Enter `^p [image number you would like to go to]`**')
            await self.Bot.remove_reaction(SearchMsg.Message, '\U0001F522', ctx.message.author)
            await self.WaitForResponse(ctx, SearchMsg)

    async def NextImage(self, SearchMsg):
        if isinstance(SearchMsg, Search):
            return False
        Embed = discord.Embed()
        if SearchMsg.Image == 99:
            SearchMsg.Image = 0
        else:
            SearchMsg.Image += 1
        Embed.set_footer(
            text='Image {} of 100'.format(SearchMsg.Image + 1))
        Embed.set_image(url=SearchMsg.Results[SearchMsg.Image]['file_url'])
        await self.Bot.edit_message(SearchMsg.Message, embed=Embed)

    async def PrevImage(self, SearchMsg):
        if isinstance(SearchMsg, Search):
            return False
        Embed = discord.Embed()
        if SearchMsg.Image == 0:
            SearchMsg.Image = 99
        else:
            SearchMsg.Image -= 1
        Embed.set_footer(
            text='Image {} of 100'.format(SearchMsg.Image + 1))
        Embed.set_image(url=SearchMsg.Results[SearchMsg.Image]['file_url'])
        await self.Bot.edit_message(SearchMsg.Message, embed=Embed)

    @commands.command(name='kona', pass_context=True, aliases=['konachan'])
    async def KonaSearch(self, ctx):
        if ctx.message.channel.id not in self.ALLOWEDCHANNELS:
            return
        if self.UserHaveSearch(ctx.message.author.id):
            await self.Bot.say(':x: **Search denied. You already have a search open.**\nEither click the \'stop\' button on that search or run `^konaforce [tags]` instead to close previous search and execute new one.')
            return
        if any(Tag in ctx.message.content for Tag in self.BLACKLISTED):
            await self.Bot.say(':x: **One of the tags you attempted to search is blacklisted.**\nBlacklisted tags: `loli` `furry`')
            return
        Parts = ctx.message.content.split(' ', 1)
        if len(Parts) < 2:
            MsgTD = await self.Bot.say(':x: **Invalid amount of arguments.**')
            await asyncio.sleep(5)
            await self.Bot.delete_messages([ctx.message, MsgTD])
        else:
            Msg = await self.Bot.say('**Please wait**\nRetrieving results')
            self.SEARCHMSGS[ctx.message.author.id] = Search(
                Msg, ctx.message.author.id, Parts[1])
            self.SEARCHMSGS[ctx.message.author.id].ExecuteSearch()
            Embed = discord.Embed()
            Embed.set_footer(text='Image 1 of 100')
            try:
                Embed.set_image(
                    url=self.SEARCHMSGS[ctx.message.author.id].Results[0]['file_url'])
            except Exception:
                await self.Bot.edit_message(Msg, '**Failed to get images, try correcting your tags**')
                del self.SEARCHMSGS[ctx.message.author.id]
                return
            await self.Bot.edit_message(Msg, new_content='Done', embed=Embed)
            await self.Bot.add_reaction(Msg, '\u2B05')
            await self.Bot.add_reaction(Msg, '\u27A1')
            await self.Bot.add_reaction(Msg, '\u23F9')
            await self.Bot.add_reaction(Msg, '\U0001F522')
            await self.WaitForResponse(ctx, self.SEARCHMSGS[ctx.message.author.id])

    @commands.command(name='page', pass_context=True, hidden=True, aliases=['p'])
    async def SetPage(self, ctx, Page: int):
        if ctx.message.author.id not in self.WaitingForPage:
            return
        if Page > 100 or Page < 1:
            MsgTD = await self.Bot.send_message(ctx.message.channel, ':x: **Invalid number entered. Image number must be a number from 1 to 100.')
            self.WaitingForPage.remove(ctx.message.author.id)
            await asyncio.sleep(5)
            await self.Bot.delete_messages([ctx.message, MsgTD])
            return
        self.SEARCHMSGS[ctx.message.author.id].Image = Page - 1
        Embed = discord.Embed()
        Embed.set_footer(text='Image {} of 100'.format(
            self.SEARCHMSGS[ctx.message.author.id].Image + 1))
        Embed.set_image(
            url=self.SEARCHMSGS[ctx.message.author.id].Results[self.SEARCHMSGS[ctx.message.author.id].Image]['file_url'])
        await self.Bot.edit_message(self.SEARCHMSGS[ctx.message.author.id].Message, embed=Embed)
        self.WaitingForPage.remove(ctx.message.author.id)
        await self.Bot.delete_message(ctx.message)

    @commands.command(name='nsfw:allowchannel', pass_context=True, aliases=['nsfw:allow'])
    async def AllowChannel(self, ctx, Channel: str):
        Success = []
        Fail = []
        AlreadyIn = []
        ChannelsFile = open('./cogs/nsfw/channels.txt', 'r+')
        Channels = [Line for Line in ChannelsFile.readlines()]
        if ctx.message.channel_mentions:
            for Channel in ctx.message.channel_mentions:
                if Channel.id in Channels:
                    AlreadyIn.append(Channel.name)
                    continue
                if Channel.permissions_for(ctx.message.author).manage_channels:
                    Success.append(Channel.name)
                    ChannelsFile.write(str(Channel.id) + '\n')
                else:
                    Fail.append(Channel.name)
        else:
            ChannelsToAdd = ctx.message.content.split(' ')
            del ChannelsToAdd[0]
            for ChannelID in ChannelsToAdd:
                Channel = ctx.message.server.get_channel(ChannelID)
                if Channel is None:
                    Fail.append(Channel.name)
                    continue
                if Channel.id in Channels:
                    AlreadyIn.append(Channel.name)
                    continue
                if Channel.permissions_for(ctx.message.author).manage_channels:
                    Success.append(Channel.name)
                    ChannelsFile.write(str(Channel.id) + '\n')
                else:
                    Fail.append(Channel.name)
        ChannelsFile.close()
        Embed = discord.Embed(
            title='Allow NSFW Features in Channel(s)', colour=0xFF1493)
        if len(Success) > 0:
            Embed.add_field(name='Successfully Allowed',
                            value=','.join(map(str, Success)))
            self.ReadChannels()
        if len(AlreadyIn) > 0:
            Embed.add_field(name='Already Allowed',
                            value=','.join(map(str, AlreadyIn)))
        if len(Fail) > 0:
            Embed.add_field(name='Failed to Add',
                            value=','.join(map(str, Fail)))
        await self.Bot.say(embed=Embed)

    @commands.command(name='nsfw:disallowchannel', pass_context=True, aliases=['nsfw:disallow'])
    async def DisallowChannel(self, ctx, Channel: str):
        Success = []
        Fail = []
        AlreadyIn = []
        ChannelsFile = open('./cogs/nsfw/channels.txt', 'r+')
        Channels = [Line for Line in ChannelsFile.readlines()]
        if ctx.message.channel_mentions:
            for Channel in ctx.message.channel_mentions:
                if Channel.id in Channels and Channel.permissions_for(ctx.message.author).manage_channels:
                    del Channels[Channels.index(Channel.id)]
                    Success.append(Channel.name)
                else:
                    Fail.append(Channel.name)
        else:
            ChannelsToAdd = ctx.message.content.split(' ')
            del ChannelsToAdd[0]
            for ChannelID in ChannelsToAdd:
                Channel = ctx.message.server.get_channel(ChannelID)
                if Channel is None:
                    Fail.append(Channel.name)
                    continue
                if Channel.id in Channels and Channel.permissions_for(ctx.message.author).manage_channels:
                    del Channels[Channels.index(Channel.id)]
                    Success.append(Channel.name)
                else:
                    Fail.append(Channel.name)
        ChannelsFile.close()
        Embed = discord.Embed(
            title='Disallow NSFW Features in Channel(s)', colour=0xFF1493)
        if len(Success) > 0:
            self.ReadChannels()
            Embed.add_field(name='Successfully Disallowed',
                            value=','.join(map(str, Success)))
        if len(AlreadyIn) > 0:
            Embed.add_field(name='Already Disallowed',
                            value=','.join(map(str, AlreadyIn)))
        if len(Fail) > 0:
            Embed.add_field(name='Failed to Disallow',
                            value=','.join(map(str, Fail)))
        await self.Bot.say(embed=Embed)

def setup(Bot):
    Bot.add_cog(NSFW(Bot))