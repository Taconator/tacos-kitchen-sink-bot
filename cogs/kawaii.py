import asyncio
import random
import time
from types import SimpleNamespace
import discord
from discord.ext import commands
from utils import checks


class Kawaii:
    SecureRandom = random.SystemRandom()
    ProtectedDict = {
        'ibi' : '249619604496711682',
        'taco' : '157619512710135808',
        'bot' : '324994650823852043',
        'aito' : '351907960382816257'
    }

    PROTECTED = SimpleNamespace(**ProtectedDict)

    HUGS = []
    POKES = []
    NOMS = []
    SELFNOMS = []
    SLAPS = []
    BOTSLAPS = []
    TICKLES = []
    LICKS = []
    SELFLICKS = []
    PATS = []
    CRYS = []
    KISSES = []
    LOLI = []
    RAGES = []
    DABS = []
    SLEEPY = []
    MURDER = []
    PUNCHES = []
    BOTPUNCHES = []

    def CountAll(self):
        return (len(self.HUGS) + len(self.POKES) + len(self.NOMS) + len(self.SELFNOMS) + len(self.SLAPS) + len(self.BOTSLAPS) + len(self.TICKLES) + len(self.LICKS) + len(self.SELFLICKS) + len(self.PATS) + len(self.CRYS) + len(self.KISSES) + len(self.LOLI) + len(self.RAGES) + len(self.SLEEPY) + len(self.MURDER))

    def __init__(self, Bot):
        self.Bot = Bot

    async def LoadImages(self, Channel):
        LoadedImages = []
        async for Message in self.Bot.logs_from(Channel, limit=500):
            LoadedImages.append(Message)
        return LoadedImages

    async def LoadHugs(self):
        self.HUGS = []
        LoadedHugs = await self.LoadImages(self.Bot.get_channel('470102894172176396'))
        for Message in LoadedHugs:
            self.HUGS.append(Message)

    async def LoadPokes(self):
        self.POKES = []
        LoadedPokes = await self.LoadImages(self.Bot.get_channel('470105175105732608'))
        for Message in LoadedPokes:
            self.POKES.append(Message)

    async def LoadNoms(self):
        self.NOMS = []
        LoadedNoms = await self.LoadImages(self.Bot.get_channel('470386291553337364'))
        for Message in LoadedNoms:
            self.NOMS.append(Message)

    async def LoadSelfNoms(self):
        self.SELFNOMS = []
        LoadedNoms = await self.LoadImages(self.Bot.get_channel('470392035472441354'))
        for Message in LoadedNoms:
            self.SELFNOMS.append(Message)

    async def LoadSlaps(self):
        self.SLAPS = []
        LoadedSlaps = await self.LoadImages(self.Bot.get_channel('470403867729854474'))
        for Message in LoadedSlaps:
            self.SLAPS.append(Message)

    async def LoadBotSlaps(self):
        self.BOTSLAPS = []
        LoadedSlaps = await self.LoadImages(self.Bot.get_channel('470426744873025547'))
        for Message in LoadedSlaps:
            self.BOTSLAPS.append(Message)

    async def LoadTickles(self):
        self.TICKLES = []
        LoadedTickles = await self.LoadImages(self.Bot.get_channel('470432972303237130'))
        for Message in LoadedTickles:
            self.TICKLES.append(Message)

    async def LoadLicks(self):
        self.LICKS = []
        LoadedLicks = await self.LoadImages(self.Bot.get_channel('470433036467568650'))
        for Message in LoadedLicks:
            self.LICKS.append(Message)

    async def LoadSelfLicks(self):
        self.SELFLICKS = []
        LoadedSelfLicks = await self.LoadImages(self.Bot.get_channel('470441275125006346'))
        for Message in LoadedSelfLicks:
            self.SELFLICKS.append(Message)

    async def LoadPats(self):
        self.PATS = []
        LoadedPats = await self.LoadImages(self.Bot.get_channel('470433288549433355'))
        for Message in LoadedPats:
            self.PATS.append(Message)

    async def LoadCrys(self):
        self.CRYS = []
        LoadedCrys = await self.LoadImages(self.Bot.get_channel('470698423436181514'))
        for Message in LoadedCrys:
            self.CRYS.append(Message)

    async def LoadKisses(self):
        self.KISSES = []
        LoadedKisses = await self.LoadImages(self.Bot.get_channel('470727837821960214'))
        for Message in LoadedKisses:
            self.KISSES.append(Message)

    async def LoadLoli(self):
        self.LOLI = []
        LoadedLoli = await self.LoadImages(self.Bot.get_channel('470807994276511764'))
        for Message in LoadedLoli:
            self.LOLI.append(Message)

    async def LoadRage(self):
        self.RAGES = []
        LoadedRages = await self.LoadImages(self.Bot.get_channel('470861931834507276'))
        for Message in LoadedRages:
            self.RAGES.append(Message)

    async def LoadDabs(self):
        self.DABS = []
        LoadedDabs = await self.LoadImages(self.Bot.get_channel('471072138699210763'))
        for Message in LoadedDabs:
            self.DABS.append(Message)

    async def LoadPunches(self):
        self.PUNCHES = []
        LoadedPunches = await self.LoadImages(self.Bot.get_channel('471072358581665793'))
        for Message in LoadedPunches:
            self.PUNCHES.append(Message)

    async def LoadSleepy(self):
        self.SLEEPY = []
        LoadedSleepy = await self.LoadImages(self.Bot.get_channel('471359836676816905'))
        for Message in LoadedSleepy:
            self.SLEEPY.append(Message)

    async def LoadMurder(self):
        self.MURDER = []
        LoadedMurder = await self.LoadImages(self.Bot.get_channel('471488734454480933'))
        for Message in LoadedMurder:
            self.MURDER.append(Message)

    async def LoadBotPunches(self):
        self.BOTPUNCHES = []
        LoadedBotPunches = await self.LoadImages(self.Bot.get_channel('472648484961648650'))
        for Message in LoadedBotPunches:
            self.BOTPUNCHES.append(Message)

    async def on_ready(self):
        Time = await self.LoadAll()
        print('Loaded all Kawaii images in {} seconds'.format(str(round(Time, 2))))

    async def LoadAll(self):
        Start = time.time()
        await self.LoadHugs()
        await self.LoadPokes()
        await self.LoadNoms()
        await self.LoadSelfNoms()
        await self.LoadSlaps()
        await self.LoadBotSlaps()
        await self.LoadTickles()
        await self.LoadLicks()
        await self.LoadSelfLicks()
        await self.LoadPats()
        await self.LoadCrys()
        await self.LoadKisses()
        await self.LoadLoli()
        await self.LoadRage()
        await self.LoadDabs()
        await self.LoadPunches()
        await self.LoadSleepy()
        await self.LoadBotPunches()
        End = time.time()
        return End - Start

    @commands.command(name='hug', description='Lets you hug other people :DDDDDDDDDDDDDDD', pass_context=True)
    async def Hug(self, ctx, UserToHug : str):
        MsgTD = None
        if len(self.HUGS) == 0:
            MsgTD = await self.Bot.say('**Please wait, loading all images**')
            Time = await self.LoadAll()
            await self.Bot.edit_message(MsgTD, new_content=':white_check_mark: **Finished loading __{}__ images in __{}__ seconds**'.format(str(self.CountAll()), Time))
        if ctx.message.mentions:
            if ctx.message.mentions[0].id == self.Bot.user.id:
                Embed = discord.Embed(color=0xffb6c1)
                Embed.set_image(url='https://vignette.wikia.nocookie.net/central/images/4/4a/Anime-comfort-hug-gif-11.gif')
                await self.Bot.say('***{}** hugs **{}** back*'.format(self.Bot.user.mention, ctx.message.author.mention), embed=Embed)
                return
            elif ctx.message.mentions[0].id == ctx.message.author.id:
                Embed = discord.Embed()
                Embed.set_image(url='https://78.media.tumblr.com/3e5e2975340aeb61a98efb9ff844c29c/tumblr_p1o87bFli41wwx4m6o1_500.gif')
                await self.Bot.say('***{}** attempts to hug themselves*\nWhy not hug me instead, i always hug back ^-^'.format(ctx.message.author.mention), embed=Embed)
                return
            Embed = discord.Embed(colour=0xffb6c1)
            Embed.set_image(url=self.SecureRandom.choice(self.HUGS).content)
            Mentions = ctx.message.mentions[0].mention
            i = 1
            while i < len(ctx.message.mentions):
                if i == (len(ctx.message.mentions) - 1):
                    Mentions += ' and {}'.format(ctx.message.mentions[i].mention)
                else:
                    Mentions += ', {}'.format(ctx.message.mentions[i].mention)
                i += 1
            await self.Bot.say('***{}** hugs **{}***'.format(ctx.message.author.mention, Mentions), embed=Embed)

    @commands.command(name='poke', description='Lets you poke other people ;P', pass_context=True)
    async def Poke(self, ctx, UserToPoke : str):
        if len(self.POKES) == 0:
            MsgTD = await self.Bot.say('**Please wait, loading all images**')
            Time = await self.LoadAll()
            await self.Bot.edit_message(MsgTD, new_content=':white_check_mark: **Finished loading __{}__ images in __{}__ seconds**'.format(str(self.CountAll()), Time))
        if ctx.message.mentions:
            if ctx.message.mentions[0].id == self.Bot.user.id:
                Embed = discord.Embed(color=0x70DBDB)
                Embed.set_image(url='https://i.pinimg.com/originals/ec/d5/db/ecd5db48f5bdfb9b67f86f2094554839.gif')
                await self.Bot.say('***{}** pokes **{}** back'.format(self.Bot.user.mention, ctx.message.author.mention), embed=Embed)
                return
            elif ctx.message.mentions[0].id == ctx.message.author.id:
                Embed = discord.Embed()
                Embed.set_image(url='http://i.imgur.com/AvV8fUf.gif')
                await self.Bot.say('***{}** attempts to poke themselves*\nCan always poke me, i always poke back ^-^'.format(ctx.message.author.mention), embed=Embed)
                return
            Embed = discord.Embed(color=0x70DBDB)
            Embed.set_image(url=self.SecureRandom.choice(self.POKES).content)
            Mentions = ctx.message.mentions[0].mention
            i = 1
            while i < len(ctx.message.mentions):
                if i == (len(ctx.message.mentions) - 1):
                    Mentions += ' and {}'.format(ctx.message.mentions[i].mention)
                else:
                    Mentions += ', {}'.format(ctx.message.mentions[i].mention)
                i += 1
            await self.Bot.say('***{}** pokes **{}***'.format(ctx.message.author.mention, Mentions), embed=Embed)

    @commands.command(name='nom', description='Lets you nom other people c:\nDELICIOUS AND NUTRITIOUS', pass_context=True)
    async def Nom(self, ctx, UserToNom : str):
        if len(self.NOMS) == 0 or len(self.SELFNOMS) == 0:
            MsgTD = await self.Bot.say('**Please wait, loading all images**')
            Time = await self.LoadAll()
            await self.Bot.edit_message(MsgTD, new_content=':white_check_mark: **Finished loading __{}__ images in __{}__ seconds**'.format(str(self.CountAll()), Time))
        if ctx.message.mentions:
            if ctx.message.mentions[0].id == self.Bot.user.id:
                Embed = discord.Embed(colour=0xffb6c1)
                Embed.set_image(url='http://i.imgur.com/L9dLr1J.gif')
                await self.Bot.say('***{}** noms **{}** back*'.format(self.Bot.user.mention, ctx.message.author.mention), embed=Embed)
                return
            elif ctx.message.mentions[0].id == ctx.message.author.id:
                Embed = discord.Embed()
                Embed.set_image(url=self.SecureRandom.choice(self.SELFNOMS).content)
                await self.Bot.say('***{}** attempts to nom themselves and instead noms \'food\'*'.format(ctx.message.author.mention), embed=Embed)
                return
            Embed = discord.Embed(color=0xffb6c1)
            Embed.set_image(url=self.SecureRandom.choice(self.NOMS).content)
            Mentions = ctx.message.mentions[0].mention
            i = 1
            while i < len(ctx.message.mentions):
                if i == (len(ctx.message.mentions) - 1):
                    Mentions += ' and {}'.format(ctx.message.mentions[i].mention)
                else:
                    Mentions += ', {}'.format(ctx.message.mentions[i].mention)
                i += 1
            await self.Bot.say('***{}** noms on **{}***'.format(ctx.message.author.mention, Mentions), embed=Embed)

    @commands.command(name='tickle', description='Lets you tickle other people ;3\nHEY THAT TICKLES', pass_context=True)
    async def Tickle(self, ctx, UserToTickle : str):
        if len(self.TICKLES) == 0:
            MsgTD = await self.Bot.say('**Please wait, loading all images**')
            Time = await self.LoadAll()
            await self.Bot.edit_message(MsgTD, new_content=':white_check_mark: **Finished loading __{}__ images in __{}__ seconds**'.format(str(self.CountAll()), Time))
        if ctx.message.mentions:
            if ctx.message.mentions[0].id == self.Bot.user.id:
                Embed = discord.Embed(color=0x70DBDB)
                Embed.set_image(url='https://uploads.disquscdn.com/images/669ec28034d7ff34a03027ec92c27fe7c9107577c9dc0671a0432814ff4cd2bc.gif')
                await self.Bot.say('***{}** tickles **{}** back*'.format(self.Bot.user.mention, ctx.message.author.mention), embed=Embed)
                return
            if ctx.message.mentions[0].id == ctx.message.author.id:
                Embed = discord.Embed()
                Embed.set_image(url='https://media1.tenor.com/images/414f5c21b4f8c70bfab422922aa3a017/tenor.gif')
                await self.Bot.say('***{}** attempts to tickle themselves*\nWhy not tickle me, I always tickle back ^-^'.format(ctx.message.author.mention), embed=Embed)
                return
            Embed = discord.Embed(color=0x70DBDB)
            Embed.set_image(url=self.SecureRandom.choice(self.TICKLES).content)
            Mentions = ctx.message.mentions[0].mention
            i = 1
            while i < len(ctx.message.mentions):
                if i == (len(ctx.message.mentions) - 1):
                    Mentions += ' and {}'.format(ctx.message.mentions[i].mention)
                else:
                    Mentions += ', {}'.format(ctx.message.mentions[i].mention)
                i += 1
            await self.Bot.say('***{}** tickles **{}***'.format(ctx.message.author.mention, Mentions), embed=Embed)

    @commands.command(name='lick', description='Lets you lick other people owo', pass_context=True)
    async def Lick(self, ctx, UserToLick : str):
        if len(self.LICKS) == 0:
            MsgTD = await self.Bot.say('**Please wait, loading all images**')
            Time = await self.LoadAll()
            await self.Bot.edit_message(MsgTD, new_content=':white_check_mark: **Finished loading __{}__ images in __{}__ seconds**'.format(str(self.CountAll()), Time))
        if ctx.message.mentions:
            if ctx.message.mentions[0].id == self.Bot.user.id:
                Embed = discord.Embed(color=0xffb6c1)
                Embed.set_image(url='https://i.kym-cdn.com/photos/images/newsfeed/001/334/768/fbc.gif')
                await self.Bot.say('***{}** licks **{}** back*'.format(self.Bot.user.mention, ctx.message.author.mention), embed=Embed)
                return
            if ctx.message.mentions[0].id == ctx.message.author.id:
                Embed = discord.Embed()
                Embed.set_image(url=self.SecureRandom.choice(self.SELFLICKS).content)
                await self.Bot.say('***{}** licks themselves*'.format(ctx.message.author.mention), embed=Embed)
                return
            Embed = discord.Embed(color=0xffb6c1)
            Embed.set_image(url=self.SecureRandom.choice(self.LICKS).content)
            Mentions = ctx.message.mentions[0].mention
            i = 1
            while i < len(ctx.message.mentions):
                if i == (len(ctx.message.mentions) - 1):
                    Mentions += ' and {}'.format(ctx.message.mentions[i].mention)
                else:
                    Mentions += ', {}'.format(ctx.message.mentions[i].mention)
                i += 1
            await self.Bot.say('***{}** licks **{}***'.format(ctx.message.author.mention, Mentions), embed=Embed)

    @commands.command(name='pat', description='Lets you pat other people :D', pass_context=True)
    async def Pat(self, ctx, UserToPat : str):
        if len(self.PATS) == 0:
            MsgTD = await self.Bot.say('**Please wait, loading all images**')
            Time = await self.LoadAll()
            await self.Bot.edit_message(MsgTD, new_content=':white_check_mark: **Finished loading __{}__ images in __{}__ seconds**'.format(str(self.CountAll()), Time))
        if ctx.message.mentions:
            if ctx.message.mentions[0].id == self.Bot.user.id:
                Embed = discord.Embed(color=0xffb6c1)
                Embed.set_image(url='https://images.gr-assets.com/hostedimages/1424840652ra/13807874.gif')
                await self.Bot.say('***{}** pats **{}** back*'.format(self.Bot.user.mention, ctx.message.author.mention), embed=Embed)
                return
            if ctx.message.mentions[0].id == ctx.message.author.id:
                Embed = discord.Embed()
                Embed.set_image(url='https://33.media.tumblr.com/bfced93c7466bb103b22e15b029753cb/tumblr_ms0sqoPAZn1r3kkyco1_500.gif')
                await self.Bot.say('***{}** pats themselves\nCould pat me, i always pat back ^-^'.format(ctx.message.author.mention), embed=Embed)
                return
            Embed = discord.Embed(color=0xffb6c1)
            Embed.set_image(url=self.SecureRandom.choice(self.PATS).content)
            Mentions = ctx.message.mentions[0].mention
            i = 1
            while i < len(ctx.message.mentions):
                if i == (len(ctx.message.mentions) - 1):
                    Mentions += ' and {}'.format(ctx.message.mentions[i].mention)
                else:
                    Mentions += ', {}'.format(ctx.message.mentions[i].mention)
                i += 1
            await self.Bot.say('***{}** pats **{}***'.format(ctx.message.author.mention, Mentions), embed=Embed)

    @commands.command(name='slap', description='Lets you slap other people >:D', pass_context=True)
    async def Slap(self, ctx, UserToSlap : str):
        if len(self.SLAPS) == 0 or len(self.BOTSLAPS) == 0:
            MsgTD = await self.Bot.say('**Please wait, loading all images**')
            Time = await self.LoadAll()
            await self.Bot.edit_message(MsgTD, new_content=':white_check_mark: **Finished loading __{}__ images in __{}__ seconds**'.format(str(self.CountAll()), Time))
        if ctx.message.mentions:
            if ctx.message.mentions[0].id == self.Bot.user.id and ctx.message.author.id == '157619512710135808':
                Embed = discord.Embed(color=0xFF0000)
                Embed.set_image(url='https://img.fireden.net/a/image/1514/03/1514037797903.gif')
                await self.Bot.say('***{taco}** slaps {bot} and {bot} accepts the slap*'.format(taco=ctx.message.author.mention, bot=self.Bot.user.mention), embed=Embed)
                return
            if ctx.message.author.id in self.PROTECTED.__dict__.values() and ctx.message.mentions[0].id in self.PROTECTED.__dict__.values():
                Embed = discord.Embed(color=0xFF0000)
                Embed.set_image(url='https://data.whicdn.com/images/119597356/original.gif')
                MsgTE = await self.Bot.say('***{}** slaps **{}***'.format(ctx.message.author.mention, ctx.message.mentions[0].mention), embed=Embed)
                await asyncio.sleep(3)
                Embed.color = 0xffb6c1
                await self.Bot.edit_message(MsgTE, new_content='***{}** slaps **{}***.......... *very lightly*'.format(ctx.message.author.mention, ctx.message.mentions[0].mention),embed=Embed)
                return
            elif ctx.message.mentions[0].id == self.PROTECTED.aito:
                Embed = discord.Embed(color=0xFF0000)
                Embed.set_image(url='https://i.stack.imgur.com/ouQ6E.gif')
                MsgTE = await self.Bot.say('***HOW DARE YOU***', embed=Embed)
                await asyncio.sleep(3)
                Embed.set_image(url=self.SecureRandom.choice(self.BOTSLAPS).content)
                await self.Bot.edit_message(MsgTE, new_content='***{}** attempts to slap {} and instead gets slapped into oblivion*'.format(ctx.message.author.mention, ctx.message.mentions[0].mention), embed=Embed)
                return
            if ctx.message.mentions[0].id in list(self.PROTECTED.__dict__.values()):
                Embed = discord.Embed(color=0xFF0000)
                Embed.set_image(url=self.SecureRandom.choice(self.BOTSLAPS).content)
                await self.Bot.say('***{}** attempts to slap {} and instead gets slapped into oblivion*'.format(ctx.message.author.mention, ctx.message.mentions[0].mention), embed=Embed)
                return
            if ctx.message.mentions[0].id == ctx.message.author.id:
                Embed = discord.Embed(color=0xFF0000)
                Embed.set_image(url='http://cdn.awwni.me/mz98.gif')
                await self.Bot.say('***{}** has slapped themselves. B- baka*'.format(ctx.message.author.mention), embed=Embed)
                return
            Embed = discord.Embed(color=0xFF0000)
            Embed.set_image(url=self.SecureRandom.choice(self.SLAPS).content)
            Mentions = ctx.message.mentions[0].mention
            i = 1
            while i < len(ctx.message.mentions):
                if i == (len(ctx.message.mentions) - 1):
                    Mentions += ' and {}'.format(ctx.message.mentions[i].mention)
                else:
                    Mentions += ', {}'.format(ctx.message.mentions[i].mention)
                i += 1
            await self.Bot.say('***{}** slaps **{}***'.format(ctx.message.author.mention, Mentions), embed=Embed)

    @commands.command(name='punch', description='Lets you punch other people >:D', pass_context=True)
    async def Punch(self, ctx, UserToPunch : str):
        await self.Bot.say('Not yet implemented, sowwy')
        return
        if len(self.PUNCHES) == 0:
            MsgTD = await self.Bot.say('**Please wait, loading all images**')
            Time = await self.LoadAll()
            await self.Bot.edit_message(MsgTD, new_content=':white_check_mark: **Finished loading __{}__ images in __{}__ seconds**'.format(str(self.CountAll()), Time))
        if ctx.message.mentions:
            if ctx.message.mentions[0].id == self.Bot.user.id and ctx.message.author.id == '157619512710135808':
                Embed = discord.Embed(color=0xFF0000)
                Embed.set_image(url='https://31.media.tumblr.com/21a047abb2d79759dad5bdc2fa4b5401/tumblr_nvqbbcpli61qfr27po1_500.gif')
                await self.Bot.say('***{taco}** punches {bot} and {bot} accepts the punch*'.format(taco=ctx.message.author.mention, bot=self.Bot.user.mention), embed=Embed)
            if ctx.message.author.id in self.PROTECTED.__dict__.values() and ctx.message.mentions[0].id in self.PROTECTED.__dict__.values():
                Embed = discord.Embed(color=0xFF0000)
                Embed.set_image(url='https://data.whicdn.com/images/119597356/original.gif')
                MsgTE = await self.Bot.say('***{}** punches **{}***'.format(ctx.message.author.mention, ctx.message.mentions[0].mention), embed=Embed)
                await asyncio.sleep(3)
                Embed.color = 0xffb6c1
                await self.Bot.edit_message(MsgTE, new_content='***{}** punches **{}***.......... *very lightly*'.format(ctx.message.author.mention, ctx.message.mentions[0].mention),embed=Embed)
                return
            elif ctx.message.mentions[0].id == self.PROTECTED.aito:
                Embed = discord.Embed(color=0xFF0000)
                Embed.set_image(url='https://i.stack.imgur.com/ouQ6E.gif')
                MsgTE = await self.Bot.say('***HOW DARE YOU***', embed=Embed)
                await asyncio.sleep(3)
                Embed.set_image(url=self.SecureRandom.choice(self.BOTSLAPS).content)
                await self.Bot.edit_message(MsgTE, new_content='***{}** attempts to punch {} and gets annihilated*'.format(ctx.message.author.mention, ctx.message.mentions[0].mention), embed=Embed)
                return
            if ctx.message.mentions[0].id == ctx.message.author.id:
                Embed = discord.Embed(color=0xFF0000)
                Embed.set_image(url='')

    @commands.command(name='sleepy', description='Lets you show other people that you\'re sleepy\n*Yawn*', pass_context=True)
    async def Sleepy(self, ctx):
        if len(self.SLEEPY) == 0:
            MsgTD = await self.Bot.say('**Please wait, loading all images**')
            Time = await self.LoadAll()
            await self.Bot.edit_message(MsgTD, new_content=':white_check_mark: **Finished loading __{}__ images in __{}__ seconds**'.format(str(self.CountAll()), Time))
        Embed = discord.Embed(color=int("%06x" % random.randint(0, 0xFFFFFF), 16))
        Embed.set_image(url=self.SecureRandom.choice(self.SLEEPY).content)
        await self.Bot.say('***{}** is sleepy*'.format(ctx.message.author.mention), embed=Embed)

    @commands.command(name='cry', description='Lets you show other people that you\'re sad :c', pass_context=True)
    async def Cry(self, ctx):
        if len(self.CRYS) == 0:
            MsgTD = await self.Bot.say('**Please wait, loading all images**')
            Time = await self.LoadAll()
            await self.Bot.edit_message(MsgTD, new_content=':white_check_mark: **Finished loading __{}__ images in __{}__ seconds**'.format(str(self.CountAll()), Time))
        Embed = discord.Embed(color=0x606060)
        Embed.set_image(url=self.SecureRandom.choice(self.CRYS).content)
        await self.Bot.say('***{}** is crying*'.format(ctx.message.author.mention),embed=Embed)

    @commands.command(name='kiss', description='Lets you kiss another person <3\nSPREAD THE LOVE', pass_context=True)
    async def Kiss(self, ctx, UserToKiss : str):
        if len(self.KISSES) == 0:
            MsgTD = await self.Bot.say('**Please wait, loading all images**')
            Time = await self.LoadAll()
            await self.Bot.edit_message(MsgTD, new_content=':white_check_mark: **Finished loading __{}__ images in __{}__ seconds**'.format(str(self.CountAll()), Time))
        if ctx.message.mentions:
            if ctx.message.mentions[0].id == self.Bot.user.id:
                Embed = discord.Embed(color=0xffb6c1)
                Embed.set_image(url='https://4.bp.blogspot.com/-3wHaSwIM6Ao/WOxXhxwkfYI/AAAAAAAAyto/qGmbxCd4CXQxajIErUoJGyeuFGknSWY6wCPcB/s1600/Omake%2BGif%2BAnime%2B-%2BFrame%2BArms%2BGirl%2B-%2BEpisode%2B2%2B-%2BGourai%2BKisses%2BStiletto%2BShock.gif')
                await self.Bot.say('***{}** kisses **{}** back*'.format(self.Bot.user.mention, ctx.message.author.mention), embed=Embed)
                return
            if ctx.message.mentions[0].id == ctx.message.author.id:
                Embed = discord.Embed()
                Embed.set_image(url='http://38.media.tumblr.com/0e9351b77fab4bf407f16a6d7e82168c/tumblr_mxih9z4wYO1slcdwbo1_500.gif')
                await self.Bot.say('***{}** attempts to kiss themselves\nWhy not kiss me, I always kiss back <3*'.format(ctx.message.author.mention), embed=Embed)
                return
            Embed = discord.Embed(color=0xffb6c1)
            BreakIn = self.SecureRandom.randint(1, 100)
            if BreakIn <= 5:
                Embed.set_image(url='https://38.media.tumblr.com/beba4d945182da9a008e263d6153585c/tumblr_nn2qcghRN11r5kj34o4_500.gif')
                MsgTE = await self.Bot.say('***{}** kisses **{}***'.format(ctx.message.author.mention, ctx.message.mentions[0].mention), embed=Embed)
                Embed.set_image(url='https://media1.tenor.com/images/93d11bc59526ce49f60766f0045d819b/tenor.gif')
                Embed.color = 0xFF0000
                await asyncio.sleep(2)
                await self.Bot.edit_message(MsgTE, new_content='**OSHIT SWAT BREAKIN IN**', embed=Embed)
                return
            Embed.set_image(url=self.SecureRandom.choice(self.KISSES).content)
            Mentions = ctx.message.mentions[0].mention
            i = 1
            while i < len(ctx.message.mentions):
                if i == (len(ctx.message.mentions) - 1):
                    Mentions += ' and {}'.format(ctx.message.mentions[i].mention)
                else:
                    Mentions += ', {}'.format(ctx.message.mentions[i].mention)
                i += 1
            await self.Bot.say('***{}** kisses **{}***'.format(ctx.message.author.mention, Mentions), embed=Embed)

    @commands.command(name='rage', description='Shows that you\'re PISSED OFF >:#', pass_context=True)
    async def Rage(self, ctx):
        if len(self.RAGES) == 0:
            MsgTD = await self.Bot.say('**Please wait, loading all images**')
            Time = await self.LoadAll()
            await self.Bot.edit_message(MsgTD, new_content=':white_check_mark: **Finished loading __{}__ images in __{}__ seconds**'.format(str(self.CountAll()), Time))
        Embed = discord.Embed(color=0x990000)
        Embed.set_image(url=self.SecureRandom.choice(self.RAGES).content)
        await self.Bot.say('***{}** is **__RAGING__***'.format(ctx.message.author.mention),embed=Embed)

    @commands.command(name='dab', description='DAB ON THE HATERS', pass_context=True)
    async def Dab(self, ctx):
        if len(self.DABS) == 0:
            MsgTD = await self.Bot.say('**Please wait, loading all images**')
            Time = await self.LoadAll()
            await self.Bot.edit_message(MsgTD, new_content=':white_check_mark: **Finished loading __{}__ images in __{}__ seconds**'.format(str(self.CountAll()), Time))
        Embed = discord.Embed(color=int("%06x" % random.randint(0, 0xFFFFFF), 16))
        Embed.set_image(url=self.SecureRandom.choice(self.DABS).content)
        await self.Bot.say('***{}** is DABBING ON THEM {}*'.format(ctx.message.author.mention, 'HATERS' if random.randint(0, 1) == 0 else 'HOS'),embed=Embed)

    @commands.command(name='loli', hidden=True, pass_context=True)
    async def Loli(self, ctx):
        if len(self.LOLI) == 0:
            MsgTD = await self.Bot.say('**Please wait, loading all images**')
            Time = await self.LoadAll()
            await self.Bot.edit_message(MsgTD, new_content=':white_check_mark: **Finished loading __{}__ images in __{}__ seconds**'.format(str(self.CountAll()), Time))
        Embed = discord.Embed(color=0xFF0000)
        Embed.set_image(url=self.SecureRandom.choice(self.LOLI).content)
        await self.Bot.say(embed=Embed)

    @commands.command(name='kwreload', hidden=True, pass_context=True)
    @checks.IsOwner()
    async def ReloadKW(self, ctx):
        MsgTD = await self.Bot.say('**Please wait, reloading all images**')
        Time = await self.LoadAll()
        await self.Bot.edit_message(MsgTD, new_content=':white_check_mark: **Finished reloading __{}__ images in __{}__ seconds**'.format(str(self.CountAll()), Time))
        await asyncio.sleep(5)
        await self.Bot.delete_messages([ctx.message, MsgTD])

    @commands.command(name='kwcount', description='Counts the number of images available to the bot for commands', pass_context=True)
    async def CountKW(self, ctx, ImageListToCount : str = 'all'):
        if len(self.PATS) == 0:
            await self.LoadAll()
        ILower = ImageListToCount.lower()
        Embed = discord.Embed(color=0x8CDD81)
        if ILower == 'all':
            Embed.description = 'Image Counts'
            Embed.add_field(name='Hugs', value='{} images\n1 self hug\n1 bot hug'.format(str(len(self.HUGS))))
            Embed.add_field(name='Pokes', value='{} images\n1 self poke\n1 bot poke'.format(str(len(self.POKES))))
            Embed.add_field(name='Noms', value='{} images\n{} self noms\n1 bot nom'.format(str(len(self.NOMS)), str(len(self.SELFNOMS))))
            Embed.add_field(name='Slaps', value='{} images\n1 self slap\n{} bot slaps'.format(str(len(self.SLAPS)), str(len(self.BOTSLAPS))))
            Embed.add_field(name='Tickles', value='{} images\n1 self tickle\n1 bot tickle'.format(str(len(self.TICKLES))))
            Embed.add_field(name='Licks', value='{} images\n{} self licks\n1 bot lick'.format(str(len(self.LICKS)), str(len(self.SELFLICKS))))
            Embed.add_field(name='Pats', value='{} images\n1 self pat\n1 bot pat'.format(str(len(self.PATS))))
            Embed.add_field(name='Crys', value='{} images'.format(str(len(self.CRYS))))
            Embed.add_field(name='Rages', value='{} images'.format(str(len(self.RAGES))))
            Embed.add_field(name='Dabs', value='{} images'.format(str(len(self.DABS))))
            Embed.add_field(name='Sleepy', value='{} images'.format(str(len(self.SLEEPY))))
            Embed.add_field(name='Secret', value='{} images'.format(str(len(self.LOLI))))
            Embed.add_field(name='Total', value=str(self.CountAll()))
        elif ILower == 'hugs' or ILower == 'hug':
            Embed.description = 'Hug Count'
            Embed.add_field(name='Hugs', value='{} images\n1 self hug\n1 bot hug'.format(str(len(self.HUGS))))
        elif ILower == 'pokes' or ILower == 'poke':
            Embed.description = 'Poke Count'
            Embed.add_field(name='Pokes', value='{} images\n1 self poke\n1 bot poke'.format(str(len(self.POKES))))
        elif ILower == 'noms' or ILower == 'nom':
            Embed.description = 'Nom Count'
            Embed.add_field(name='Noms', value='{} images\n{} self noms\n1 bot nom'.format(str(len(self.NOMS)), str(len(self.SELFNOMS))))
        elif ILower == 'slaps' or ILower == 'slap':
            Embed.description = 'Slap Count'
            Embed.add_field(name='Slaps', value='{} images\n1 self slap\n{} bot slaps'.format(str(len(self.SLAPS)), str(len(self.BOTSLAPS))))
        elif ILower == 'tickles' or ILower == 'tickle':
            Embed.description = 'Tickle Count'
            Embed.add_field(name='Tickles', value='{} images\n1 self tickle\n1 bot tickle'.format(str(len(self.TICKLES))))
        elif ILower == 'licks' or ILower == 'lick':
            Embed.description = 'Lick Count'
            Embed.add_field(name='Licks', value='{} images\n{} self licks\n1 bot lick'.format(str(len(self.LICKS)), str(len(self.SELFLICKS))))
        elif ILower == 'pats' or ILower == 'pat':
            Embed.description = 'Pat Count'
            Embed.add_field(name='Pats', value='{} images\n1 self pat\n1 bot pat'.format(str(len(self.PATS))))
        elif ILower == 'crys' or ILower == 'cry':
            Embed.description = 'Cry Count'
            Embed.add_field(name='Crys', value='{} images'.format(str(len(self.CRYS))))
        elif ILower == 'rages' or ILower == 'rage':
            Embed.description = 'Rage Count'
            Embed.add_field(name='Rages', value='{} images'.format(str(len(self.RAGES))))
        elif ILower == 'dabs' or ILower == 'dab':
            Embed.description = 'Dab Count'
            Embed.add_field(name='Dabs', value='{} images'.format(str(len(self.DABS))))
        elif ILower == 'sleepy':
            Embed.description = 'Sleepy Count'
            Embed.add_field(name='Sleepy', value='{} images'.format(str(len(self.SLEEPY))))
        elif ILower == 'murder':
            Embed.description = 'Murder Count'
            Embed.add_field(name='Murder', value='{} images'.format(str(len(self.MURDER))))
        else:
            MsgTD = await self.Bot.say(':x: **Invalid image list specified.**')
            await asyncio.sleep(5)
            await self.Bot.delete_messages([ctx.message, MsgTD])
            return
        await self.Bot.say(embed=Embed)


def setup(Bot):
    Bot.add_cog(Kawaii(Bot))