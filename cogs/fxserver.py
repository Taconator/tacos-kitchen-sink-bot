import base64
import json
import urllib.request


class InfoGetter:
    @staticmethod
    def Get(IP : str, Port : int):
        try:
            Data = None
            with urllib.request.urlopen(urllib.request.Request(f'http://{IP}:{Port}/info.json')) as Response:
                Data = json.loads(Response.read().decode())
            return Data
        except Exception:
            return False

    @staticmethod
    def ToMarkdown(Data: dict):
        if 'server' not in Data:
            return False
        Out = '```markdown\nServer Info\n===========\n\n'

    @staticmethod
    def DecodeIcon(Base64Icon : str):
        Image = open('CurrentIcon.png', 'wb')
        Image.write(base64.b64decode(Base64Icon))
        Image.close()
        return 'CurrentIcon.png'


class PlayersGetter:
    @staticmethod
    def Get(IP : str, Port : int):
        try:
            Data = None
            with urllib.request.urlopen(urllib.request.Request(f'http://{IP}:{Port}/players.json')) as Response:
                Data = json.loads(Response.read().decode())
            return Data
        except Exception:
            return False

    @staticmethod
    def ToMarkdown(Data : dict):
        Out = '```markdown\nPlayers\n=======\n\n'
        i = 0
        while i < len(Data):
            Out += f'{i + 1}.\t[{Data[i]["id"]}][{Data[i]["name"]}][{Data[i]["identifiers"]}] - {Data[i]["ping"]} ping'
            i += 1
        return Out + 'This shit empty```' if len(Data) == 0 else '```'
