import asyncio
import datetime
import os
import random
from colormap import rgb2hex
from colormap import hex2rgb
import discord
import numexpr as ne
from discord.ext import commands
from PIL import Image


class Random:

    OWNERID = '157619512710135808'

    Secure_Random = random.SystemRandom()

    HELLORESPONSES = [
        'Hai{}',
        'Hoi{}',
        'Hiyah{} :3',
        'Hi, how ya doing {}?'
    ]

    BOREDRESPONSES = [
        'try playing some games',
        'try talking to people',
        'try socializing',
        'try going longboarding',
        'try going skateboarding',
        'try masturbating, o.o wat',
        'try jacking off, o.o wat',
        'try going surfboarding',
        'try talking to me c:',
        'try learning another language',
        'try watching a video',
        'try sleeping, i like to sleep :3',
        'try taking a nap, i like taking naps :3',
        'try making a Discord bot (like me :DDDDD)',
        'if you like drifting and have GTA V, try downloading FiveM and joining The Tofu Shop (shameless plug :P)',
        'if you YOU CAN, try going for a drive',
        'try going for a walk'
    ]

    def __init__(self, Bot):
        self.Bot = Bot

    @commands.command(name='reverse', pass_context=True)
    async def Reverse(self, ctx, TextToReverse : str):
        await self.Bot.say(ctx.message.content.split(' ', 1)[1][::-1])

    @commands.command(name='russianroulette', pass_context=True, aliases=['rr'])
    async def RussianRoulette(self, ctx, Bullets : str):
        try:
            BulletsInt = int(Bullets)
        except (TypeError, ValueError):
            MsgTD = await self.Bot.say(':x: **Number of bullets needs to be a number.**')
            await asyncio.sleep(5)
            await self.Bot.delete_messages([ctx.message, MsgTD])
            return
        if BulletsInt == 45:
            Msg = await self.Bot.say(':cheese: :gun: **.45 WITH CHEESE loaded**')
            await asyncio.sleep(random.randint(1, 11))
            await self.Bot.edit_message(Msg, '**BIG SMOKE MOTHERFUCKER**\nhttps://vignette.wikia.nocookie.net/gtawiki/images/b/bd/BigSmoke-GTASA.jpg/revision/latest?cb=20100105192204')
        elif (BulletsInt <= -1 or BulletsInt > 6):
            MsgTD = await self.Bot.say(':x: **Number of bullets need to be between 0 (inclusive) and 6 (also inclusive).**')
            await asyncio.sleep(5)
            await self.Bot.delete_messages([ctx.message, MsgTD])
        elif BulletsInt == 6:
            Msg = await self.Bot.say(':sob: :gun: **6 bullets loaded**')
            await asyncio.sleep(random.randint(1, 11))
            await self.Bot.edit_message(Msg, ':boom: **Ded**')
        else:
            Emoji = ':frowning2:' if BulletsInt > 0 else ':upside_down'
            if BulletsInt > 1 or BulletsInt == 0:
                Msg = await self.Bot.say('{} :gun: **{} bullets loaded'.format(Emoji, Bullets))
            else:
                Msg = await self.Bot.say('{} :gun: **{} bullet loaded'.format(Emoji, Bullets))
            await asyncio.sleep(random.randint(1, 11))
            if random.randint(1, 7) <= BulletsInt:
                await self.Bot.edit_message(Msg, ':boom: **Ded**')
            else:
                await self.Bot.edit_message(Msg, ':grinning: **Not ded**')

    @commands.command(name='game', pass_context=True)
    async def Game(self, ctx, Member : str):
        if not ctx.message.mentions:
            User = await self.Bot.get_user_info(Member)
            if User is not False:
                Out = '**{}** is currently playing **{}**'.format(User.name, User.game.name) if User.game is not None else '{} either isn\'t playing anything or doesn\'t share a server with the bot'.format(User.name)
                await self.Bot.say(Out)
            else:
                MsgTD = await self.Bot.say(':x: **Could not find user with ID {}.**'.format(Member))
                await asyncio.sleep(5)
                await self.Bot.delete_messages([ctx.message, MsgTD])
        else:
            await self.Bot.say('**{}** is currently playing **{}**'.format(ctx.message.mentions[0].name, ctx.message.mentions[0].game.name) if ctx.message.mentions[0].game is not None else '**{}** either isn\'t playing anything currently')

    @commands.command(name='hi', aliases=['hey', 'hello', 'hai', 'hiyah', 'hoi'], pass_context=True)
    async def Hello(self, ctx):
        Secure_Random = random.SystemRandom()
        Tilde = '~' if random.randint(0, 1) == 1 else ''
        await self.Bot.say('{}, <@{}>'.format(Secure_Random.choice(self.HELLORESPONSES).format(Tilde), ctx.message.author.id))

    @commands.command(name='bored', aliases=['im bored', 'i\'m bored'], pass_context=True)
    async def Bored(self, ctx):
        Secure_Random = random.SystemRandom()
        await self.Bot.say('hmm, {} <@{}>'.format(Secure_Random.choice(self.BOREDRESPONSES), ctx.message.author.id))

    @commands.command(name='thank', pass_context=True)
    async def Thank(self, ctx):
        if 'you' in ctx.message.content:
            await self.Bot.say('**uwu**')
            await self.Bot.send_typing(ctx.message.channel)
            await asyncio.sleep(2)
            await self.Bot.say('You\'e welcome! c:')
        else:
            await self.Bot.say('Thank..... what exactly? o.o')

    @commands.command(name='imsorry', pass_context=True, aliases=['i\'msorry'])
    async def Sorry(self, ctx):
        await self.Bot.say('Uhh, dont feel bad')
        await self.Bot.send_typing(ctx.message.channel)
        await asyncio.sleep(2)
        await self.Bot.say('Is ok ^-^')

    @commands.command(name='good job', pass_context=True, aliases=['gj'])
    async def GoodJob(self, ctx):
        await self.Bot.say('Thank you, ' + ctx.message.author.display_name)
        await self.Bot.send_typing(ctx.message.channel)
        await asyncio.sleep(2)
        await self.Bot.say(':3')

    @commands.command(name='trash', pass_context=True)
    async def Trash(self, ctx):
        await self.Bot.say('https://giphy.com/gifs/frank-place-BmgBRaw1GH0MU')
        await self.Bot.delete_message(ctx.message)

    @commands.command(name='fros', pass_context=True, hidden=True)
    async def Fros(self, ctx):
        await self.Bot.say('**USELESS**')
        await self.Bot.delete_message(ctx.message)

    @commands.command(name='peasants', pass_context=True, hidden=True, aliases=['peasant'])
    async def Peasant(self, ctx):
        await self.Bot.say('**AND IF THE WORLD EVER HAS AN APOCALYPSE\nI WILL KILL ALL OF YOU FUCKERS\nFEAR WILL BE PLENTIFUL, DEATH WILL BE BOUNTIFUL\nI WILL SPARE NONE OF YOU PEASANTS**')
        await self.Bot.delete_message(ctx.message)

    @commands.command(name='color', pass_context=True, aliases=['colour'])
    @commands.cooldown(1, 2, commands.BucketType.server)
    async def Color(self, ctx, ColorToShow: str):
        if ColorToShow.lower() == 'random':
            Hex = '0x' + ("%06x" % self.Secure_Random.randint(0, 0xFFFFFF)).upper()
            RGB = hex2rgb(Hex)
            Msg = '**Hex:** {}\n**RGB:** {}'.format(Hex, str(RGB))
            Img = Image.new('RGB', (500, 500), RGB)
            Filepath = 'C:\\users\\bunta\\desktop\\botimages\\color.png'
            Img.save(Filepath)
            await self.Bot.send_file(ctx.message.channel, fp=Filepath, content=Msg)
            os.remove(Filepath)
            return
        try:
            if ColorToShow.startswith('#') or ColorToShow.startswith('0x'):
                RGB = hex2rgb(ColorToShow)
                Msg = '**Hex:** {}\n**RGB:** {}'.format(ColorToShow, str(RGB))
                Img = Image.new('RGB', (500, 500), RGB)
                Filepath = 'C:\\users\\bunta\\desktop\\botimages\\color.png'
                Img.save(Filepath)
                await self.Bot.send_file(ctx.message.channel, fp=Filepath, content=Msg)
                os.remove(Filepath)
            else:
                r, g, b = ctx.message.content.replace('(', '').replace(')', '').split(' ', 1)[1].strip().split(',')
                if r == None or g == None or b == None:
                    await self.Bot.say(':x: **Could not split up rgb values. A good RGB value is `(255, 0, 0)` or `255, 0, 0`.**')
                    return
                Hex = rgb2hex(int(r), int(g), int(b))
                Msg = '**RGB:** {}\n**Hex:** {}'.format(ctx.message.content.split(' ', 1)[1], str(Hex))
                Img = Image.new('RGB', (500, 500), (int(r), int(g), int(b)))
                Filepath = 'C:\\users\\bunta\\desktop\\botimages\\color.png'
                Img.save(Filepath)
                await self.Bot.send_file(ctx.message.channel, fp=Filepath, content=Msg)
                os.remove(Filepath)
        except Exception as E:
            await self.Bot.say('*I\'m too lazy to implement error handling so here\'s a generic error message*\n\n' + str(E))
            return

    @commands.command(name='sippintea', pass_context=True)
    async def Sippin(self, ctx):
        await self.Bot.say('**BITCH I\'M SIPPIN TEA IN YO HOOD, WHAT THE FUCK IS UP**')
        await self.Bot.delete_message(ctx.message)

    @commands.command(name='attachments', pass_context=True)
    async def Attachments(self, ctx, Channel : str, MessageID : int):
        Message = await self.Bot.get_message(ctx.message.channel_mentions[0], str(MessageID))
        await self.Bot.say(Message.attachments[0]['url'])

    @commands.command(name='coin', pass_context=True, aliases=['flip'])
    async def CoinFlip(self, ctx):
        Embed = discord.Embed(color=int("%06x" % random.randint(0, 0xFFFFFF), 16))
        Embed.set_image(url='https://cdn.discordapp.com/attachments/472883797692776458/474472075097014283/tumblr_np6oolnI2c1td4t64o1_500.gif')
        Embed.set_author(name='{} is flipping a coin'.format(ctx.message.author.display_name), icon_url=ctx.message.author.default_avatar_url if ctx.message.author.avatar_url is None else ctx.message.author.avatar_url)
        MsgTD = await self.Bot.say(embed=Embed)
        await asyncio.sleep(3)
        await self.Bot.delete_message(MsgTD)
        await self.Bot.say('**You got {}, {}**'.format('tails' if self.Secure_Random.randint(0, 1) == 1 else 'heads', ctx.message.author.mention))


    @commands.command(name='whois', pass_context=True)
    @commands.cooldown(1, 5, commands.BucketType.server)
    async def Whois(self, ctx):
        if not ctx.message.mentions:
            Parts = ctx.message.content.split(' ', 1)
            if len(Parts) == 1:
                Member = ctx.message.author
            else:
                Member = ctx.message.server.get_member(Parts[1])
                if Member is None:
                    try:
                        Member = await self.Bot.get_user_info(Parts[1])
                    except discord.NotFound:
                        await self.Bot.say(':x: **Failed to get user from ID or mentions.**')
                        return
        else:
            Member = ctx.message.mentions[0]
        Roles = []
        RolesString = 'None'
        Game = 'Nothing'
        if isinstance(Member, discord.Member):
            Status = Member.status if str(Member.status).lower() != 'dnd' else 'do not disturb'
            if len(Member.roles) > 0:
                for Role in Member.roles:
                    if not 'everyone' in Role.name:
                        Roles.append(Role.name)
            if Member.game is not None:
                Game = Member.game.name
        if len(Roles) != 0:
            RolesString = ', '.join(Roles)
        Avatar = Member.default_avatar_url
        if Member.avatar_url is not None:
            Avatar = Member.avatar_url
        Embed = discord.Embed(color = Member.color if (isinstance(Member, discord.Member)) else int("%06x" % random.randint(0, 0xFFFFFF), 16))
        Embed.set_thumbnail(url = Avatar)
        if Member.id == self.OWNERID:
            Embed.description = 'Creator of This Bot'
        Embed.set_author(name = 'Who Is: ' + Member.name, icon_url = Avatar)
        Embed.add_field(name = 'User', value='{}#{}'.format(Member.name, Member.discriminator))
        Embed.add_field(name = 'ID', value = Member.id)
        if isinstance(Member, discord.Member):
            Embed.add_field(name = 'Display Name', value=Member.display_name)
            Embed.add_field(name = 'Status', value = str(Status).title())
            Embed.add_field(name = 'Joined At', value = str(Member.joined_at))
            Embed.add_field(name = 'Account Created At', value = str(Member.created_at))
            Embed.add_field(name = 'Role(s) ({})'.format(len(Roles)), value = RolesString)
            Embed.add_field(name = 'Owner of Server', value = str('{' + str(Member == ctx.message.server.owner) + '}').lower().format(true = 'Yes', false='No'))
        Embed.add_field(name = 'Bot', value = str('{' + str(Member.bot) + '}').lower().format(true = 'Yes', false = 'No'))
        Embed.add_field(name = 'Game', value = Game)
        await self.Bot.say(embed = Embed)
        return

    @commands.command(name='calc', pass_context=True)
    async def Calc(self, ctx, Expression : str):
        Exp = ctx.message.content.split(' ', 1)[1].replace('^', '**')
        await self.Bot.say('`{}` is __{}__'.format(Exp, str(ne.evaluate(ex=Exp))))

    @commands.command(name='getfucked', pass_context=True)
    async def GetFucked(self, ctx):
        await self.Bot.say('https://youtu.be/vvMnITx0xqA?t=10s')

    async def on_message(self, Message):
        DVSHAMING = [
            'dv',
            'd/v',
            'v/d',
            '/vd',
            'vd',
            'dv/',
            'vd/',
            '/dv]',
            '/dvb',
            '/dvc',
            '/dvd',
            '/dvv',
            '/ddv'
        ]

        if Message.channel.id == '434593216038174750' and Message.author.bot:
            MLower = Message.content.lower()
            if MLower in DVSHAMING:
                Embed = discord.Embed(title='{} failed to type /dv correctly'.format(Message.author.display_name), colour=int("%06x" % random.randint(0, 0xFFFFFF), 16), timestamp=datetime.datetime.now())
                Embed.add_field(name='Offending Message', value=Message.content)
                Embed.add_field(name='Congratulations', value='You have the big gay')
                await self.Bot.send_message(Message.channel.server.get_channel('467864940359712768'), embed=Embed)
        if 'leveled up' in Message.content.lower() and Message.author.bot:
            await self.Bot.send_message(Message.channel, '*ding*')


def setup(Bot):
    Bot.add_cog(Random(Bot))