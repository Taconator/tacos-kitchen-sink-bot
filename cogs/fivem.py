import collections
import json
import urllib.request
from discord.ext import commands


class FiveM:
    ServerInfo = {
        'xongames 1' : '*xon 1 also works*\nxONGames Drift Server Modded Maps +200 modded cars,maps,peds,scripts\n\n5.189.151.33:30125',
        'xongames ls' : '*xon ls also works*\nxONGames Drift Server LS + Akina +200 modded cars,maps,peds,scripts\n\n5.189.151.33:30120',
        'dragon drive' : '*`dd` also works*\nDragon Drive! [Custom maps!] [100+ custom cars!] [Eurobeat Radio!] [Realistic handling + Drift toggle]\n\n104.251.211.230:30120',
        'the tofu shop' : '*`ttfs` also works*\nThe Tofu Shop |Mountain Racing | Touge | Custom Vehicles | discord.gg/fwCEvpx\n\n149.56.184.98:30120',
        'velocity drift servers ls 1' : '*`vds ls` and `vds ls 1` also work*\n► Velocity Drift Servers :: Los Santos Drift #1 :: Custom Content & Cars ◄\n\n145.239.4.113:30120',
        'velocity drift servers ls 2' : '*`vds ls 2` also works*\n► Velocity Drift Servers :: Los Santos Drift #2 :: Custom Content & Cars ◄\n\n145.239.4.113:30123',
        'velocity drift servers ls 3' : '*`vds ls 3` also works*\n► Velocity Drift Servers :: Los Santos Drift #2 :: Custom Content & Cars ◄\n\n145.239.4.113:30126',
        'velocity drift servers tracks 1' : '*`vds 1`, `vds` and `vds tracks 1` also work*\n► Velocity Drift Servers :: Tracks Drift #1 :: Custom Content & Cars ◄\n\n145.239.4.113:30121',
        'velocity drift servers tracks 2' : '*`vds 2` and `vds tracks 2` also work*\n► Velocity Drift Servers :: Tracks Drift #2 :: Custom Content & Cars ◄\n\n145.239.4.113:30124',
        'velocity drift servers tracks 3' : '*`vds 3` and `vds tracks 3` also work*\n► Velocity Drift Servers :: Tracks Drift #3 :: Custom Content & Cars ◄\n\n145.239.4.113:30127'
    }

    SERVERS = collections.OrderedDict()

    def __init__(self, Bot):
        self.Bot = Bot
        self.InitServers()

    def InitServers(self):
        for Key, Value in self.ServerInfo.items():
            self.SERVERS[Key] = Value.split('\n\n', 1)[1]
        self.SERVERS['dd'] = self.SERVERS['dragon drive']
        self.SERVERS['xon 1'] = self.SERVERS['xongames 1']
        self.SERVERS['xon ls'] = self.SERVERS['xongames ls']
        self.SERVERS['ttfs'] = self.SERVERS['the tofu shop']
        self.SERVERS.update({'vds ls' : self.SERVERS['velocity drift servers ls 1'], 'vds ls 1' : self.SERVERS['velocity drift servers ls 1']})
        self.SERVERS['vds ls 2'] = self.SERVERS['velocity drift servers ls 2']
        self.SERVERS['vds ls 3'] = self.SERVERS['velocity drift servers ls 3']
        self.SERVERS.update({'vds' : self.SERVERS['velocity drift servers tracks 1'], 'vds 1' : self.SERVERS['velocity drift servers tracks 1'], 'vds tracks 1' : self.SERVERS['velocity drift servers tracks 1']})
        self.SERVERS.update({'vds 2' : self.SERVERS['velocity drift servers tracks 2'], 'vds tracks 2' : self.SERVERS['velocity drift servers tracks 2']})
        self.SERVERS.update({'vds 3' : self.SERVERS['velocity drift servers tracks 3'], 'vds tracks 3' : self.SERVERS['velocity drift servers tracks 3']})

    def Servers(self, String):
        String = String.lower()
        String = String.replace('vds ls 3', self.SERVERS['vds ls 3'])
        String = String.replace('vds ls 2', self.SERVERS['vds ls 2'])
        String = String.replace('vds ls 1', self.SERVERS['vds ls'])
        String = String.replace('vds ls', self.SERVERS['vds ls'])
        String = String.replace('vds 2', self.SERVERS['vds 2'])
        String = String.replace('vds 3', self.SERVERS['vds 3'])
        String = String.replace('velocity drift servers ls 3', self.SERVERS['vds ls 3'])
        String = String.replace('velocity drift servers ls 2', self.SERVERS['vds ls 2'])
        String = String.replace('velocity drift servers ls 1', self.SERVERS['vds ls'])
        String = String.replace('velocity drift servers tracks 1', self.SERVERS['vds'])
        String = String.replace('velocity drift servers tracks 2', self.SERVERS['vds 2'])
        String = String.replace('velocity drift servers tracks 3', self.SERVERS['vds 3'])
        String = String.replace('vds 1', self.SERVERS['vds'])
        String = String.replace('vds 2', self.SERVERS['vds 2'])
        String = String.replace('vds 3', self.SERVERS['vds 3'])
        String = String.replace('vds tracks 1', self.SERVERS['vds'])
        String = String.replace('vds tracks 2', self.SERVERS['vds 2'])
        String = String.replace('vds tracks 3', self.SERVERS['vds 3'])
        for Key, Value in reversed(self.SERVERS.items()):
            String = String.replace(Key, Value)
        return String

    @classmethod
    def GetServerData(self, IP, Port):
        try:
            Data = None
            with urllib.request.urlopen(urllib.request.Request(f'http://{IP}:{Port}/players.json')) as Response:
                Data = json.loads(Response.read().decode())
            Out = '```markdown\nPlayers\n=======\n\n'
            i = 0
            while i < len(Data):
                Out += '{}.\t[{}][{}][{}] - {} ping\n'.format(i + 1, Data[i]['id'], Data[i]['name'], Data[i]['identifiers'][0], Data[i]['ping'])
                i += 1
            if len(Data) == 0:
                Out += 'This shit empty'
            Out += '```'
            return Out
        except Exception:
            return False

    @classmethod
    def GetServerDataNormal(self, IP, Port):
        try:
            Data = None
            with urllib.request.urlopen(urllib.request.Request(f'http://{IP}:{Port}/players.json')) as Response:
                Data = json.loads(Response.read().decode())
            Out = '```markdown\nPlayers\n=======\n\n'
            i = 0
            while i < len(Data):
                Out += '{}.\t[{}][{}] - {} ping\n'.format(i + 1, Data[i]['id'], Data[i]['name'], Data[i]['ping'])
                i += 1
            if len(Data) == 0:
                Out += 'This shit empty'
            Out += '```'
            return Out
        except Exception:
            return False

    @classmethod
    def GetServerInfoNormal(self, IP, Port):
        try:
            Data = None
            with urllib.request.urlopen(urllib.request.Request(f'http://{IP}:{Port}/info.json')) as Response:
                Data = json.loads(Response.read().decode())
        except Exception:
            pass

    @classmethod
    def GetServerDataNoFormatting(self, IP, Port):
        try:
            with urllib.request.urlopen('http://{}:{}/players.json'.format(IP, Port)) as URL:
                return json.loads(URL.read().decode())
        except Exception:
            return False

    @classmethod
    def GetServerInfo(self, IP, Port):
        try:
            Data = None
            with urllib.request.urlopen('http://{}:{}/info.json'.format(IP, Port)) as URL:
                Data = json.loads(URL.read().decode())
                return Data
        except Exception:
            return False

    @commands.command(name='fivem:up', pass_context=True)
    async def FiveMUp(self, ctx):
        FormattedMsg = self.Servers(ctx.message.content)
        Parts = FormattedMsg.split(' ', 1)
        await self.Bot.send_typing(ctx.message.channel)
        if len(Parts) > 1:
            IP, Port = Parts[1].split(':', 1)
            Port = Port.split(' ', 1)[0]
            Out = self.GetServerDataNoFormatting(IP, Port)
            if Out == False:
                await self.Bot.say(':x: **Server is __down__**')
            else:
                await self.Bot.say(':white_check_mark: **Server is up and has __{} players__ currently**'.format(len(Out)))
        else:
            await self.Bot.say(':x: **Not enough arguments.**')

    @commands.command(name='fivem:players', pass_context=True)
    async def FiveMPlayers(self, ctx):
        FormattedMsg = self.Servers(ctx.message.content)
        Parts = FormattedMsg.split(' ', 1)
        await self.Bot.send_typing(ctx.message.channel)
        if len(Parts) > 1:
            IP, Port = Parts[1].split(':', 1)
            Port = Port.split(' ', 1)[0]
            Out = self.GetServerDataNormal(IP, Port)
            if Out == False:
                await self.Bot.say(':x: **Failed to get players\nServer is probably down**')
            else:
                await self.Bot.say(Out)

    @commands.command(name='fivem:info', pass_context=True)
    async def FiveMInfo(self, ctx):
        IP, Port = ctx.message.split(' ', 1)[1].split(':', 1)

    @commands.command(name='printdict', pass_context=True)
    async def PrintDict(self, ctx):
        await self.Bot.say(list(reversed(self.SERVERS.keys())))


def setup(Bot):
    Bot.add_cog(FiveM(Bot))