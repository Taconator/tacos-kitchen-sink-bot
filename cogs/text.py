import random
from discord.ext import commands


class Text:

    BOLDREPLACEMENTS = {
        "a" : "𝐚",
        "b" : "𝐛",
        "c" : "𝐜",
        "d" : "𝐝",
        "e" : "𝐞",
        "f" : "𝐟",
        "g" : "𝐠",
        "h" : "𝐡",
        "i" : "𝐢",
        "j" : "𝐣",
        "k" : "𝐤",
        "l" : "𝐥",
        "m" : "𝐦",
        "n" : "𝐧",
        "o" : "𝐨",
        "p" : "𝐩",
        "q" : "𝐪",
        "r" : "𝐫",
        "s" : "𝐬",
        "t" : "𝐭",
        "u" : "𝐮",
        "v" : "𝐯",
        "w" : "𝐰",
        "x" : "𝐱",
        "y" : "𝐲",
        "z" : "𝐳",
        "A" : "𝐀",
        "B" : "𝐁",
        "C" : "𝐂",
        "D" : "𝐃",
        "E" : "𝐄",
        "F" : "𝐅",
        "G" : "𝐆",
        "H" : "𝐇",
        "I" : "𝐈",
        "J" : "𝐉",
        "K" : "𝐊",
        "L" : "𝐋",
        "M" : "𝐌",
        "N" : "𝐍",
        "O" : "𝐎",
        "P" : "𝐏",
        "Q" : "𝐐",
        "R" : "𝐑",
        "S" : "𝐒",
        "T" : "𝐓",
        "U" : "𝐔",
        "V" : "𝐕",
        "W" : "𝐖",
        "X" : "𝐗",
        "Y" : "𝐘",
        "Z" : "𝐙",
        "1" : "𝟏",
        "2" : "𝟐",
        "3" : "𝟑",
        "4" : "𝟒",
        "5" : "𝟓",
        "6" : "𝟔",
        "7" : "𝟕",
        "8" : "𝟖",
        "9" : "𝟗",
        "0" : "𝟎"
    }

    def __init__(self, Bot):
        self.Bot = Bot

    @commands.command(name='bold', pass_context=True)
    async def Bold(self, ctx, TextToBold : str):
        Parts = ctx.message.content.split(' ', 1)
        for Key, Value in self.BOLDREPLACEMENTS.items():
            Parts[1] = Parts[1].replace(Key, Value)
        await self.Bot.say(Parts[1])
        await self.Bot.delete_message(ctx.message)

    @commands.group(pass_context=True, name='case')
    async def Case(self, ctx):
        if ctx.invoked_subcommand is None:
            await self.Bot.say(':x: **No subcommand entered**')
            return

    @Case.command(name='lower', pass_context=True)
    async def Lower(self, ctx, TextToLower : str):
        Parts = ctx.message.content.split(' ', 2)
        await self.Bot.say(Parts[2].lower())
        await self.Bot.delete_message(ctx.message)

    @Case.command(name='upper', pass_context=True)
    async def Upper(self, ctx, TextToUpper : str):
        Parts = ctx.message.content.split(' ', 2)
        await self.Bot.say(Parts[2].upper())
        await self.Bot.delete_message(ctx.message)

    @Case.command(name='random', pass_context=True)
    async def Random(self, ctx, TextToRandomize : str):
        await self.Bot.say(''.join([char.lower() if random.randint(0, 1) == 0 else char.upper() for char in ctx.message.content.split(' ', 2)[2]]))
        await self.Bot.delete_message(ctx.message)

    @Case.command(name='inverse', pass_context=True, aliases=['invert'])
    async def Invert(self, ctx, TextToInvert : str):
        await self.Bot.say(ctx.message.content.split(' ', 2)[2].swapcase())
        await self.Bot.delete_message(ctx.message)


def setup(Bot):
    Bot.add_cog(Text(Bot))