import asyncio
import os
import random
import discord
from discord.ext import commands
from utils import checks


class Owner:
    LockedChannel = {
        'Destination' : None,
        'Source' : None
    }

    CHEESERS = [
        "293827258748108801"
    ]

    ICECUBELINES = [
        'I\'M KNOCKING NIGGAS OUT THE BOX DAILY',
        'SO WHEN I\'M IN YOUR NEIGHBORHOOD YOU BETTER DUCK CUZ ICE CUBE IS CRAZY AS FUCK',
        'FUCK THE PO-PO ACTIN\' LIKE DEEBO',
        'A 9 IS TERRIBLE IN YOUR FACE',
        'RACIST MOTHERFUCKER',
        'BLACK POLICE SHOWIN\' OUT FOR THE WHITE COP',
        'WHITE POLICE SHOWIN\' OUT FOR THE BLACK COP',
        'WE CAN GO TOE TO TOE IN THE MIDDLE OF A CELL',
        'SHOTGUN BULLETS ARE BAD FOR YOUR HEALTH',
        'I COME REAL STEALTH, DROPPING BOMBS ON YOUR MOMS',
        'BIG DICKS IN YOUR ASS IS BAD FOR YOUR HEALTH',
        'SMOKIN\' COCAINE JUST TO MAINTAIN',
        'BLACK LIVES MATTER IT\'S NOT CHIT CHATTER BECAUSE ALL THEY WANNA DO IS SCATTER BRAIN MATTER',
        'IF I SELL A LITTLE CRACK, AIN\'T NOTHING TO IT, GANGSTA RAP MADE ME DO IT',
        'CALLIN\' ME ARNOLD BUT YOU BENT-A-DICK, EAZY-E SAW YOUR ASS AND WENT IN IT QUICK',
        'TRIED TO DISS ICE CUBE IT WASN\'T WORTH IT CAUSE THE BROOMSTICK FIT YOUR ASS SO PERFECT',
        'GANGBANGED BY YOUR MANAGER FELLA, GETTING MONEY OUT YO ASS, LIKE A MOTHAFUCKIN READY TELLER',
        'ERIC WRIGHT, PUNK, ALWAYS INTO SOMETHING, GETTIN FUCKED AT NIGHT BY MISTA SHITPACKER, BEND OVER FOR THE GODDAMN CRACKER NO VASELINE',
        'YOU\'RE GETTING FUCKED REAL QUICK, AND EAZY\'S DICK IS SMELLING LIKE MC REN\'S SHIT',
        'I STARTED OFF WITH TOO MUCH CARGO, DROPPED FOUR NIGGAS NOW I\'M MAKING ALL THE DOUGH'
    ]

    SecureRandom = random.SystemRandom()

    def __init__(self, Bot):
        self.Bot = Bot

    @commands.command(name='setgame', hidden=True, pass_context=True, aliases=['sg', 'cg'])
    @checks.IsOwner()
    async def SetGame(self, ctx, Game : str):
        Parts = ctx.message.content.split(' ', 1)
        if len(Parts) < 2:
            MsgTD = await self.Bot.say(':x: **Invalid amount of arguments entered. Do `^help sg` for more information.**')
            await asyncio.sleep(5)
            await self.Bot.delete_messages([ctx.message, MsgTD])
        else:
            await self.Bot.change_presence(game=discord.Game(name=Parts[1]))
            MsgTD = await self.Bot.say(':white_check_mark: **Bot game is now set to `{}`.**'.format(Parts[1]))
            await asyncio.sleep(5)
            await self.Bot.delete_messages([ctx.message, MsgTD])

    @commands.command(name='setstatus', hidden=True, pass_context=True, aliases=['ss', 'cs'])
    @checks.IsOwner()
    async def SetStatus(self, ctx, Status : str):
        Parts = ctx.message.content.lower().split(' ', 1)
        Game = ctx.message.server.get_member(self.Bot.user.id).game
        if len(Parts) < 2:
            MsgTD = await self.Bot.say(':x: **Invalid amount of arguments entered. Do `^help sg` for more information.**')
        else:
            if Parts[1] == '1' or Parts[1] == 'online':
                NewStatus = 'online'
                await self.Bot.change_presence(status=discord.Status.online, game=Game)
            elif Parts[1] == '2' or Parts[1] == 'away' or Parts[1] == 'idle':
                NewStatus = 'idle'
                await self.Bot.change_presence(status=discord.Status.idle, game=Game)
            elif Parts[1] == '3' or Parts[1] == 'dnd' or Parts[1] == 'do not disturb':
                NewStatus = 'do not disturb'
                await self.Bot.change_presence(status=discord.Status.dnd, game=Game)
            elif Parts[1] == '4' or Parts[1] == 'invisible' or Parts[1] == 'offline':
                NewStatus = 'invisible'
                await self.Bot.change_presence(status=discord.Status.invisible, game=Game)
            else:
                MsgTD = await self.Bot.say(':x: **Invalid status `{}`.**'.format(Parts[1]))
                await asyncio.sleep(5)
                await self.Bot.delete_messages([ctx.message, MsgTD])
                return
            MsgTD = await self.Bot.say(':white_check_mark: **Bot status set to {}.**'.format(NewStatus))
        await asyncio.sleep(5)
        await self.Bot.delete_messages([ctx.message, MsgTD])

    @commands.command(name='instantshutdown', hidden=True, pass_context=True, aliases=['is'])
    @checks.IsOwner()
    async def InstantShutdown(self, ctx):
        await self.Bot.close()

    @commands.command(name='exitwithwarning', hidden=True, pass_context=True)
    @checks.IsOwner()
    async def ExitWarning(self, ctx):
        MsgTD = await self.Bot.say('Goodbye, bot is going to sleep in about 5 seconds ~')
        await asyncio.sleep(5)
        await self.Bot.delete_messages([MsgTD, ctx.message])
        await self.Bot.close()

    @commands.command(name='glade', hidden=True, pass_context=True)
    @checks.IsOwner()
    async def Glade(self, ctx):
        await self.Bot.say("The joy of whole room freshness")
        await asyncio.sleep(2)
        await self.Bot.say("Glade plug-in oil scented fan")
        await asyncio.sleep(1)
        await self.Bot.say("**IT'S ALL AROUND YOU, YOU DON'T EVEN KNOW IT**")
        await asyncio.sleep(2)
        await self.Bot.say("It's the only freshener to use a continuous, built-in fan")
        await asyncio.sleep(1)
        await self.Bot.say("**IT'S JUST THERE IN THE AIR**")
        await asyncio.sleep(2)
        await self.Bot.say( "Unlike some fresheners that seem strong just around the outlet, the Glade fan continuously circulates fragrance throughout your whole room")
        await asyncio.sleep(1)
        await self.Bot.say("**THIS SHIT WILL BLOW YOUR MOTHERFUCKIN MIND**")
        await self.Bot.say("**MAGIC ERRYWHERE IN THIS BITCH**")
        await self.Bot.say("Plug-in scented oil fan from Glade")
        await self.Bot.say("**PURE MOTHERFUCKING MAGIC**")

    @commands.command(name='gladejamaican', hidden=True, pass_context=True)
    @checks.IsOwner()
    async def GladeJamaican(self, ctx):
        await self.Bot.say("Di joy of whole room freshness")
        await asyncio.sleep(2)
        await self.Bot.say("Glade plug-in oil scented fan")
        await asyncio.sleep(1)
        await self.Bot.say("**IT ALL ROUND YUH YUH NUH EVEN KNO IT**")
        await asyncio.sleep(2)
        await self.Bot.say("it di ongle freshena to use a continuous built-in fan")
        await asyncio.sleep(1)
        await self.Bot.say("**IT JUST THERE INNA DI AIR**")
        await asyncio.sleep(2)
        await self.Bot.say("Unlike sum freshenas dat seem strong just round di outlet di Glade fan continuously circulates fragrance throughout yuh whole room")
        await asyncio.sleep(1)
        await self.Bot.say("**DIS SHIT WI BLOW YUH MOTHERFUCKIN MIND **")
        await self.Bot.say("**MAGIC ERRYWHERE INNA DIS BITCH**")
        await self.Bot.say("Plug-in scented oil fan from Glade")
        await self.Bot.say("**PURE MOTHERFUCKING MAGIC**")

    @commands.command(name='sendhere', hidden=True, pass_context=True, aliases=['sh'])
    @checks.IsOwner()
    async def SendHere(self, ctx):
        MsgTD = await self.Bot.say(ctx.message.content.split(' ', 1)[1])
        await self.Bot.delete_message(ctx.message)
        await asyncio.sleep(3)
        await self.Bot.delete_message(MsgTD)

    @commands.command(name='sendto', hidden=True, pass_context=True, aliases=['st'])
    @checks.IsOwner()
    async def SendTo(self, ctx):
        if ctx.message.channel_mentions:
            await self.Bot.send_message(ctx.message.channel_mentions[0], ctx.message.content.split(' ', 2)[2])
            await self.Bot.delete_message(ctx.message)
        else:
            Parts = ctx.message.content.split(' ', 2)
            await self.Bot.send_message(ctx.message.server.get_channel(Parts[1]), Parts[2])
            await self.Bot.delete_message(ctx.message)

    @commands.command(name='cheese', hidden=True, pass_context=True)
    @checks.IsOwner()
    async def Cheese(self, ctx):
        await self.Bot.say('Cheese?')
        await asyncio.sleep(2)
        await self.Bot.say('Did you say cheese?')
        await asyncio.sleep(0.5)
        await self.Bot.say('*Big Smoke intensifies*')
        await asyncio.sleep(2)
        await self.Bot.say('CHEESE?!')
        await asyncio.sleep(1)
        await self.Bot.say('CHEESE?!')
        await asyncio.sleep(1)
        await self.Bot.say('CHEESE!')
        await asyncio.sleep(1)
        await self.Bot.say('CHEESE!')

    @commands.command(name='icecube', hidden=True, pass_context=True)
    async def IceCube(self, ctx):
        if ctx.message.author.id == '157619512710135808' or ctx.message.author.id in self.CHEESERS:
            await self.Bot.say('**{}**'.format(self.SecureRandom.choice(self.ICECUBELINES)))

    @commands.command(name='restart', hidden=True, pass_context=True)
    @checks.IsOwner()
    async def Restart(self, ctx):
        os.startfile('C:\\Users\\bunta\Desktop\\Bunta\'s Kitchen Sink Bot\\Rewrite\\main.py')
        await self.Bot.close()

    @commands.command(name='lockchannel', hidden=True, pass_context=True)
    @checks.IsOwner()
    async def LockChannel(self, ctx):
        if ctx.message.channel_mentions:
            self.LockedChannel['Source'] = ctx.message.channel
            self.LockedChannel['Destination'] = ctx.message.channel_mentions[0]
            MsgTD = await self.Bot.say(f':white_check_mark: **Locked message channel to #{self.LockedChannel["Destination"].name}**\nAny messages sent in the current channel will be sent there')
            await asyncio.sleep(5)
            await self.Bot.delete_messages([MsgTD, ctx.message])
            return
        else:
            Parts = ctx.message.content.split(' ', 1)
            self.LockedChannel['Source'] = ctx.message.channel
            self.LockedChannel['Destination'] = ctx.message.server.get_channel(Parts[1])
            MsgTD = await self.Bot.say(f':white_check_mark: **Locked message channel to #{self.LockedChannel["Destination"].name}**\nAny messages sent in the current channel will be sent there')
            await asyncio.sleep(5)
            await self.Bot.delete_messages([MsgTD, ctx.message])

    @commands.command(name='lockchannelexternal', hidden=True, pass_context=True, aliases=['lockchannelex', 'lex'])
    @checks.IsOwner()
    async def LockChannelExternal(self, ctx):
        Parts = ctx.message.content.split(' ', 2)
        Server = self.Bot.get_server(Parts[1])
        if Server is None:
            MsgTD = await self.Bot.say(':x: **Server not found.**')
            await asyncio.sleep(5)
            await self.Bot.delete_messages([ctx.message, MsgTD])
            return
        Channel = Server.get_channel(Parts[2])
        if Channel is None:
            MsgTD = await self.Bot.say(':x: **Channel not found.**')
            await asyncio.sleep(5)
            await self.Bot.delete_messages([ctx.message, MsgTD])
            return
        self.LockedChannel['Source'] = ctx.message.channel
        self.LockedChannel['Destination'] = Channel
        MsgTD = await self.Bot.say(f':white_check_mark: **Locked message channel to #{self.LockedChannel["Destination"].name} in {self.LockedChannel["Destination"].server.name}**\nAny messages sent in the current channel will be sent there')
        await asyncio.sleep(5)
        await self.Bot.delete_messages([MsgTD, ctx.message])

    @commands.command(name='unlockchannel', hidden=True, pass_context=True)
    @checks.IsOwner()
    async def UnlockChannel(self, ctx):
        self.LockedChannel = {
            'Destination' : None,
            'Source' : None
        }
        MsgTD = await self.Bot.say(f':white_check_mark: **Unlocked message channel, you may now send messages in the channel that was previously locked**')
        await asyncio.sleep(5)
        await self.Bot.delete_messages([MsgTD, ctx.message])


    async def on_message(self, Message):
        if Message.channel != self.LockedChannel['Source'] or Message.content.startswith('^'):
            return
        try:
            checks.IsOwnerCheck(Message)
            Formatted = Message.content.replace('\\^', '^')
            await self.Bot.send_message(self.LockedChannel['Destination'], Formatted)
            await self.Bot.delete_message(Message)
        except checks.NotOwner:
            return  
        
def setup(Bot):
    Bot.add_cog(Owner(Bot))